<?php

/**
 * @file
 * Definition of rwf_handler_sort_date.
 */

/**
 * Rwf sort handler for dates.
 */
class rwf_handler_sort_date extends views_handler_sort_date {

  /**
   * @return array
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * @param $form
   * @param $form_state
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Called to add the sort to a query.
   */
  function query() {

    $field_add = "(SELECT COALESCE((select max(rwf.changed) from rwf_workflow rwf where node.nid = rwf.nid group by rwf.nid), node.changed))";

    switch ($this->options['granularity']) {
      case 'second':
      default:
        // Add the field
        $this->query->add_field(NULL, $field_add, "last_change");
        // Add the order
        $this->query->add_orderby(NULL, NULL, $this->options['order'], "last_change");
        return;
      case 'minute':
        $formula = views_date_sql_format('YmdHi', "$field_add");
        break;
      case 'hour':
        $formula = views_date_sql_format('YmdH', "$field_add");
        break;
      case 'day':
        $formula = views_date_sql_format('Ymd', "$field_add");
        break;
      case 'month':
        $formula = views_date_sql_format('Ym', "$field_add");
        break;
      case 'year':
        $formula = views_date_sql_format('Y', "$field_add");
        break;
    }

    // Add the field.
    $this->query->add_orderby(NULL, $formula, $this->options['order'], $this->table_alias . '_' . $this->field . '_' . $this->options['granularity']);
  }
}
