<?php

/**
 * Class rwf_handler_filter_has_workflow
 */
class rwf_handler_filter_has_workflow extends views_handler_filter_boolean_operator {

  /**
   *
   */
  function query() {
    if ($this->value) {
      $this->query->add_where_expression(0, "nid IN (select rw.nid from rwf_workflow rw where node.nid = rw.nid)");
    }
  }

}
