<?php
/**
 * @file
 * Definition of rwf_handler_field_title_link.
 */

/**
 * Description of what my handler does.
 */
class rwf_handler_field_title_link extends views_handler_field_entity {

  /**
   * @return array
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  /**
   * @param $form
   * @param $form_state
   */
  function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display (if only the node is in workflow)'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  /**
   * @param $values
   * @return mixed
   */
  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_link($entity, $values);
    }
  }

  /**
   * @param $node
   * @param $values
   * @return mixed
   */
  function render_link($node, $values) {
    // TODO: node_access('view', $node)?!
    $this->options['alter']['make_link'] = TRUE;


    // Check if type of node is in workflow
    // control the variable 'rwf_enable_[$node->type]' is set 1
    if (variable_get('rwf_enable_' . $node->type, 0) == 1) {

      // TODO: in future can become "node/$node->nid/versions" for rwf
      $this->options['alter']['path'] = "node/$node->nid";
      $text = !empty($this->options['text']) ? $this->options['text'] : $node->title;
    }
    else {

      $text = $node->title;

      // Check rabbit_hole
      if (module_exists('rh_node')) {

        // Check bypass rh_node
        if (!user_access('bypass rh_node')) {
          
          // returns false if there are no redirect or page_not_found configuration
          if (rabbit_hole_get_action('node', $node) != FALSE) {
            $this->options['alter']['path'] = "node/$node->nid/edit";
            return $text;
          }
        }
      }

      // node not in workflow and not redirect from rabbit_hole
      $this->options['alter']['path'] = "node/$node->nid";
    }
    return $text;

  }
}
