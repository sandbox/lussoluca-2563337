<?php

/**
 * Class rwf_handler_filter_state
 */
class rwf_handler_filter_state extends views_handler_filter_in_operator {

  /**
   *
   */
  function query() {
    if (empty($this->value)) {
      return;
    }

    $node_table = $this->ensure_my_table();
    $rwf_table = '{' . $this->query->ensure_table('rwf_workflow') . '}';

    $unpublishedStates = explode(',', variable_get('rwf_workflow_unpublished_states'));
    $coalesceExpression = "(select COALESCE(concat('s:', rwf_scheduling.state), rwf_workflow.state))";

    // join rwf_workflow to get the version not yet published
    $sub_query = db_select('rwf_workflow');
    $sub_query->fields('rwf_workflow');
    $sub_query->addExpression($coalesceExpression, 'defstate');
    $sub_query->condition('rwf_workflow.state', $unpublishedStates, 'IN');
    $sub_query->leftJoin( 'rwf_scheduling', 'rwf_scheduling', "rwf_scheduling.wid = rwf_workflow.wid");

    $join1 = new views_join();
    $join1->definition = array('table formula' => $sub_query, 'left_field' => 'nid', 'field' => 'nid', 'left_table' => 'node');
    $join1->left_table = 'node';
    $join1->field = 'nid';
    $join1->left_field = 'nid';
    $join1->type = 'LEFT';

    $this->query->add_relationship('rw2',$join1,'node');

    // join rwf_workflow to get the latest version already published
    $subquery_wg = db_select('rwf_workflow');
    $subquery_wg->addExpression('MAX(rwf_workflow.wid)', 'maxwid');
    $subquery_wg->condition('state', $unpublishedStates, 'NOT IN');
    $subquery_wg->groupBy('rwf_workflow.nid');

    $sub_query2 = db_select('rwf_workflow');
    $sub_query2->fields('rwf_workflow');
    $sub_query2->addExpression($coalesceExpression, 'defstate');
    $sub_query2->join($subquery_wg, 'wg', 'rwf_workflow.wid = wg.maxwid');
    $sub_query2->leftJoin( 'rwf_scheduling', 'rwf_scheduling', "rwf_scheduling.wid = rwf_workflow.wid");

    $join2 = new views_join();
    $join2->definition = array('table formula' => $sub_query2, 'left_field' => 'nid', 'field' => 'nid', 'left_table' => 'node');
    $join2->left_table = 'node';
    $join2->field = 'nid';
    $join2->left_field = 'nid';
    $join2->type = 'LEFT';

    $this->query->add_relationship('rw1',$join2,'node');


    // the condition depends on the operator
    if ($this->operator === 'in') {
      $firstState = array_keys($this->value)[0];
      $secondState = array_keys($this->value)[1];
      if ($secondState === null) {
        $where_expression = "((rw1.defstate = '$firstState') OR (rw2.defstate = '$firstState'))";
      } else {
        $where_expression = "((rw1.defstate = '$firstState' AND rw2.defstate = '$secondState') OR (rw1.defstate = '$secondState' AND rw2.defstate = '$firstState'))";
      }
    } else {
      $states = '(\'' . implode('\',\'', array_keys($this->value)) . '\')';
      $where_expression = "rw1.defstate in $states OR rw2.defstate in $states";
    }

    $this->query->add_where_expression(NULL, $where_expression);
  }

  /**
   * @return array
   */
  function operators() {
    $operators = array(
      'not in' => array(
        'title' => t('Almeno uno'),
        'short' => t('or'),
        'short_single' => t('<>'),
        'method' => 'op_simple',
        'values' => 1,
      ),
      'in' => array(
        'title' => t('Lista esatta'),
        'short' => t('and'),
        'short_single' => t('='),
        'method' => 'op_simple',
        'values' => 1,
      ),
    );

    return $operators;
  }

  /**
   *
   */
  public function get_value_options() {

    // TODO: replace '- Any -' with 'Filtro per Stato di workflow...',

    $availableStates = array(
      'draft' => t('draft', array(), array('context' => 'rwf')),
      'review' => t('review', array(), array('context' => 'rwf')),
      'published' => t('published', array(), array('context' => 'rwf')),
      'unpublished' => t('unpublished', array(), array('context' => 'rwf')),
      'archived' => t('archived', array(), array('context' => 'rwf')),
      's:published' => t('s:published', array(), array('context' => 'rwf')),
      's:archived' => t('s:archived', array(), array('context' => 'rwf')),
      's:unpublished' => t('s:unpublished', array(), array('context' => 'rwf')),
    );

    $this->value_options = $availableStates;
  }

}
