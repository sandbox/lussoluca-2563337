<?php

/**
 * Class rwf_handler_filter_author
 */
class rwf_handler_filter_author extends views_handler_filter_user_name {

  function query() {
    if ($this->value) {
      $placeholder = $this->placeholder();
      $this->query->add_where_expression(0, "node.uid in ($placeholder) or node.nid in (select ro.nid from rwf_operation ro where node.nid = ro.nid and ro.uid in ($placeholder))", array($placeholder => $this->value));
    }
  }

}
