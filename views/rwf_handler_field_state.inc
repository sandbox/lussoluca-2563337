<?php

use Drupal\rwf\Workflow\NodeStateDescription;

/**
 * Class rwf_handler_field_state
 */
class rwf_handler_field_state extends views_handler_field {

  /**
   *
   */
  function query() {
    $unpublishedStates = explode(',', variable_get('rwf_workflow_unpublished_states'));
    $filter = "'" . implode("', '", $unpublishedStates) . "'";

    $this->ensure_my_table();
    $this->field_alias = $this->query->add_field(NULL, "(select COALESCE(concat('s:', sch.state), rwf.state) from rwf_workflow rwf left outer join rwf_scheduling sch on rwf.wid = sch.wid where node.nid = rwf.nid and rwf.state not in ($filter) order by rwf.wid desc limit 1)", 'last_published');
    $this->field_alias = $this->query->add_field(NULL, "(select COALESCE(concat('s:', sch.state), rwf.state) from rwf_workflow rwf left outer join rwf_scheduling sch on rwf.wid = sch.wid where node.nid = rwf.nid and rwf.state in ($filter) order by rwf.wid desc limit 1)", 'last_unpublished');
    $this->add_additional_fields();
  }

  /**
   * @param $values
   * @return string
   */
  function render($values) {
    $description = new NodeStateDescription($values->last_unpublished, $values->last_published);
    return t($description->__toString(), array(), array('context' => 'rwf'));
  }
}
