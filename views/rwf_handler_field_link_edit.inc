<?php
/**
 * @file
 * Definition of rwf_handler_field_link_edit.
 */

/**
 * Field handler to present a link node edit based of node-type is in workflow or not.
 */
class rwf_handler_field_link_edit extends views_handler_field_node_link {

  /**
   * Renders the edit link.
   *
   * @param $node
   * @param $values
   *
   * @return string
   */
  function render_link($node, $values) {
    $text = '';

    // Check if type of node is in workflow
    // control the variable 'rwf_enable_[$node->type]' is set 1
    if (variable_get('rwf_enable_' . $node->type, 0) == 1) {
      if (rwf_is_user_allowed($node)) {
        $this->options['alter']['path'] = "node/$node->nid/versions";
        $text = !empty($this->options['text']) ? $this->options['text'] : t('list versions');
      }
    }
    else {
      // Normal node edit link
      $this->options['alter']['path'] = "node/$node->nid/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    }
    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['query'] = drupal_get_destination();
    return $text;
  }
}
