<?php

/**
 * Class rwf_handler_filter_date
 */
class rwf_handler_filter_date extends date_views_filter_handler_simple {

  /**
   * Alter the query.
   * commented because alter the function 'op_simple'.
   * I leave as a reference or alternative method of resolution.
   */
//  function query() {
//
//    // get value
//    $value = $this->get_filter_value('value', $this->value['value']);
//
//    // get operator
//    $operator = $this->operator;
//
//    // create where expression custom
//    $where_expression = "(COALESCE((select max(rwf_workflow.changed) from rwf_workflow where node.nid = rwf_workflow.nid group by rwf_workflow.nid), node.changed))";
//
//    // format field (= expression custom) to sql format
//    $replace = array(
//      'Y' => '%Y',
//      'y' => '%y',
//      'M' => '%b',
//      'm' => '%m',
//      'n' => '%c',
//      'F' => '%M',
//      'D' => '%a',
//      'd' => '%d',
//      'l' => '%W',
//      'j' => '%e',
//      'W' => '%v',
//      'H' => '%H',
//      'h' => '%h',
//      'i' => '%i',
//      's' => '%s',
//      'A' => '%p',
//      '\WW' => 'W%U',
//    );
//    $format = strtr($this->format, $replace); // replace work only for 'mysql' and 'mysqli'
//    $field = "DATE_FORMAT(FROM_UNIXTIME($where_expression), '$format')";
//    $where_expression = "($field $operator '$value' )";
//
//    // add where expression
//    $this->query->add_where_expression(NULL, $where_expression);
//  }

  /**
   * {@inheritdoc}
   */
  function op_simple($field) {

    // Change field from 'node.workflow_date' to custom-query/field
    $field = "(COALESCE((select max(rwf_workflow.changed) from rwf_workflow where node.nid = rwf_workflow.nid group by rwf_workflow.nid), node.changed))";

    $value = $this->get_filter_value('value', $this->value['value']);
    $comp_date = new DateObject($value, date_default_timezone(), $this->format);
    $field = $this->date_handler->sql_field($field, NULL, $comp_date);
    $field = $this->date_handler->sql_format($this->format, $field);
    $placeholder = $this->placeholder();
    $group = !empty($this->options['date_group']) ? $this->options['date_group'] : $this->options['group'];
    $this->query->add_where_expression($group, "$field $this->operator $placeholder", array($placeholder => $value));
  }

  /**
   * {@inheritdoc}
   */
  function operators() {
    $operators = parent::operators();

    // remove operations not yet supported
    unset($operators['between']);
    unset($operators['not between']);
    unset($operators['regular_expression']);
    unset($operators['contains']);
    return $operators;
  }

}
