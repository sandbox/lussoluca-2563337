<?php

/**
 * Implements hook_views_data().
 */
function rwf_views_data() {
  $data = array();

  $data['node']['rwf_title_link'] = array(
    'title' => t('Title Link Revision Workflow'),
    'help' => t('Link to revision page if node is in workflow.'),
    'group' => t('Revision Workflow'),
    'field' => array(
      'handler' => 'rwf_handler_field_title_link',
    ),
  );

  $data['node']['rwf_edit_link'] = array(
    'title' => t('Edit link (Revision Workflow)'),
    'help' => t('Link to list revision page if node is in workflow.'),
    'group' => t('Revision Workflow'),
    'field' => array(
      'handler' => 'rwf_handler_field_link_edit',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_query_alter().
 */
function rwf_views_query_alter(&$view, &$query) {
  if ($view->name != 'my_content') {
    return;
  }
}

/**
 * Implements hook_views_data_alter().
 */
function rwf_views_data_alter(&$data) {
  $data['node']['has_workflow']['title'] = t('Has workflow');
  $data['node']['has_workflow']['help'] = t('Display only nodes managed by a workflow');
  $data['node']['has_workflow']['filter']['handler'] = 'rwf_handler_filter_has_workflow';
  $data['node']['has_workflow']['group'] = t('Revision Workflow');

  $data['node']['workflow_state']['title'] = t('State');
  $data['node']['workflow_state']['help'] = t('State of the most relevant versions');
  $data['node']['workflow_state']['field']['handler'] = 'rwf_handler_field_state';
  $data['node']['workflow_state']['filter']['handler'] = 'rwf_handler_filter_state';
  $data['node']['workflow_state']['group'] = t('Revision Workflow');

  $data['node']['workflow_date']['title'] = t('Last update time');
  $data['node']['workflow_date']['help'] = t('Time of the most recent operation on any version of this node');
  $data['node']['workflow_date']['field']['handler'] = 'rwf_handler_field_date';
  $data['node']['workflow_date']['filter']['handler'] = 'rwf_handler_filter_date';
  $data['node']['workflow_date']['sort']['handler'] = 'rwf_handler_sort_date';
  $data['node']['workflow_date']['group'] = t('Revision Workflow');

  $data['node']['has_workflow']['title'] = t('Author of any revision');
  $data['node']['has_workflow']['help'] = t('Author who contributed to any revision');
  $data['node']['has_workflow']['filter']['handler'] = 'rwf_handler_filter_author';
  $data['node']['has_workflow']['group'] = t('Revision Workflow');

}

/**
 * Implements hook_views_handlers().
 */
function rwf_views_handlers() {
  return array(
    'handlers' => array(
      'rwf_handler_filter_has_workflow' => array(
        'parent' => 'views_handler_filter',
      ),
      'rwf_handler_field_date' => array(
        'parent' => 'views_handler_field_date',
      ),
      'rwf_handler_filter_date' => array(
        'parent' => 'views_handler_filter_date',
      ),
      'rwf_handler_sort_date' => array(
        'parent' => 'views_handler_sort_date',
      ),
      'rwf_handler_field_state' => array(
        'parent' => 'views_handler_field',
      ),
      'rwf_handler_filter_state' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'rwf_handler_filter_state' => array(
        'parent' => 'views_handler_filter_user_name',
      ),
    ),
  );
}
