<?php

namespace Drupal\rwf\Workflow;

/**
 * A GuardInterface implementation that checks if a user is allowed to manage a
 * node depending on its roles.
 *
 * @see Transition
 */
class DrupalRoleGuard extends RoleGuard {

  /**
   * @param array $context
   * @return bool
   * @throws \Exception if $context doesn't contain 'node' and 'user' keys
   */
  public function allow($context = array()) {

    if ($this->basicCheck($context)) {
      return TRUE;
    }

    return count(array_intersect($context['user']->roles, $this->allowedRoles)) > 0;
  }

}
