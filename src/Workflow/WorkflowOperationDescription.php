<?php

namespace Drupal\rwf\Workflow;


use Drupal\rwf\DTO\WorkflowOperation;
use Drupal\rwf\StateMachine\WorkflowManager;
use Wellnet\StateMachine\StateMachineEvents;

/**
 * Wraps a WorkflowOperation instance to provide a user-friendly description
 * @package Drupal\rwf\Workflow
 *
 * @see WorkflowOperation
 */
class WorkflowOperationDescription {

  /** @var WorkflowOperation */
  private $workflowOperation;

  function __construct(WorkflowOperation $workflowOperation) {
    $this->workflowOperation = $workflowOperation;
  }

  /**
   * Returns the description of an operation. The generic format is:
   *
   * from_state -> to_state
   * optional_info
   *
   * but it is frequently ignored to provide more descriptive messages.
   *
   * @return string
   */
  public function __toString() {
    $wo = $this->workflowOperation;

    // custom descriptions
    if ($wo->forked_from) {
      $description = t($wo->state, array(), array('context' => 'rwf')) . ' (a partire da VID ' . $wo->forked_from . ')';
    }
    elseif ($wo->event_type === WorkflowManager::EVENT_TRANSITION_UNSCHEDULED) {
      $description = 'Schedulazione annullata';
    }
    elseif ($wo->event_type === WorkflowManager::EVENT_UNPUBLISH_OLD_VERSIONS) {
      $description = 'Versione rimpiazzata da una più recente';
    }
      elseif ($wo->event_type === StateMachineEvents::START) {
      $description = 'Nuova versione';
    }

    // descriptions with generic pattern
    else {
      if ($wo->event_type === ScheduledTransitionExecutor::EVENT_TRANSITION_EXECUTED) {
        $description = 'Scheduled';
      }
      elseif ($wo->scheduling_time) {
        $description = t($wo->state, array(), array('context' => 'rwf'));
      }
      else {
        $description = t($wo->old_state, array(), array('context' => 'rwf'));
      }
      $description .= ' -> ';
      if ($wo->event_type === 'workflow-manager.transition.scheduled') {
        $description .= 'Scheduled ' . t($wo->input, array(), array('context' => 'rwf')) . ' in data ' . format_date($wo->scheduling_time, 'custom', 'd/m/Y');
      }
      else {
        $description .= t($wo->state, array(), array('context' => 'rwf'));
      }
    }
    return $description;
  }

}
