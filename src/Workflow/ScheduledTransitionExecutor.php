<?php

namespace Drupal\rwf\Workflow;


use Drupal\rwf\DAO\WorkflowDAO;
use Drupal\rwf\DAO\WorkflowSchedulingDAO;
use Drupal\rwf\DAO\WorkflowOperationDAO;
use Drupal\rwf\DTO\DTOUtils;
use Drupal\rwf\DTO\Workflow;
use Drupal\rwf\DTO\WorkflowOperation;
use Drupal\rwf\StateMachine\VersionsManager;

/**
 * Class responsible of executing all scheduled transitions.
 *
 * @package Drupal\rwf\Workflow
 */
class ScheduledTransitionExecutor {

  CONST EVENT_TRANSITION_EXECUTED = 'scheduled-transition-executor.transition.succeeded';

  /** @var WorkflowDAO */
  private $workflowDAO;

  /** @var WorkflowSchedulingDAO */
  private $workflowSchedulingDAO;

  /** @var WorkflowOperationDAO */
  private $workflowOperationDAO;

  /** @var VersionsManager */
  private $versionsManager;

  function __construct(WorkflowDAO $workflowDAO, WorkflowSchedulingDAO $workflowSchedulingDAO, WorkflowOperationDAO $workflowOperationDAO, VersionsManager $versionsManager) {
    $this->workflowDAO = $workflowDAO;
    $this->workflowSchedulingDAO = $workflowSchedulingDAO;
    $this->workflowOperationDAO = $workflowOperationDAO;
    $this->versionsManager = $versionsManager;
  }

  /**
   * Executes all scheduled transitions.
   *
   * @param null $hour overrides the hour stored in the rwf_scheduling table
   */
  public function execute($hour = NULL) {

    $schedules = $this->workflowSchedulingDAO->getAllSchedules();

    // executes schedules independently
    foreach ($schedules as $scheduling) {

      // if the hour has been overridden, check if the schedule has to be
      // executed
      if (isset($hour)) {
        if (REQUEST_TIME < mktime($hour['hour'], $hour['minute'], $hour['second'], date('n', $scheduling->scheduling_time), date('j', $scheduling->scheduling_time), date('Y', $scheduling->scheduling_time))) {
          continue;
        }
      }

      // update the Workflow DTO
      /** @var Workflow $workflow */
      $workflow = $this->workflowDAO->findByWid($scheduling->wid);
      $workflow->state = $scheduling->state;
      $workflow->changed = REQUEST_TIME;
      $this->workflowDAO->saveOrUpdate($workflow);

      $this->versionsManager->executeCollateralOperations($workflow->nid, $workflow->vid, $workflow->state);

      // remove the scheduling just performed
      $this->workflowSchedulingDAO->deleteScheduling($scheduling);

      // log the operation
      $workflowOperation = new WorkflowOperation();
      $workflowOperation->event_type = self::EVENT_TRANSITION_EXECUTED;
      $workflowOperation->wid = $workflow->wid;
      DTOUtils::fillWorkflowOperation($workflowOperation, $workflow);
      $this->workflowOperationDAO->save($workflowOperation);
    }

  }

}
