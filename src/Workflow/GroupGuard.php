<?php

namespace Drupal\rwf\Workflow;

/**
 * A GuardInterface implementation that checks if a user is allowed to manage a
 * node depending on its OG roles.
 * If the node doesn't belong to a group, the check is considered positive.
 *
 * @see Transition
 */
class GroupGuard extends RoleGuard {

  /**
   * @param array $context
   * @return bool
   * @throws \Exception if $context doesn't contain 'node' and 'user' keys
   */
  public function allow($context = array()) {

    if ($this->basicCheck($context)) {
      return TRUE;
    }

    // get the groups to which the node belongs
    $groups = og_get_entity_groups('node', $context['node']);
    if (empty($groups)) {
      return TRUE;
    }
    $nodeGroups = $groups['node'];

    $isAllowed = FALSE;

    // get the groups to which the user belongs
    $userGroups = $this->getGroupsByUser($context['user']);

    // check if the user can manage this node
    if (count(array_intersect($userGroups, $nodeGroups)) > 0) {
      // check if the transitions is restricted to some roles
      if (isset($this->allowedRoles)) {
        // checks if the user has one of the OG roles required to perform this
        // operation
        $isAllowed = $this->hasRole($context['user'], $nodeGroups, $this->allowedRoles);
      }
      else {
        $isAllowed = TRUE;
      }
    }

    return $isAllowed;
  }

  /**
   * Returns true if the account has an og role in this group (or in one of the
   * parent group of this group).
   *
   * @param $account
   * @param $groups
   * @param $allowedRoles
   *
   * @return bool
   */
  private function hasRole($account, $groups, $allowedRoles) {

    // Does the user has allowed roles in the first level parents?
    foreach ($groups as $group) {
      $roles = og_get_user_roles('node', $group, $account->uid);
      if(count(array_intersect(array_unique($roles), $allowedRoles)) > 0) {
        return TRUE;
      }
    }

    // Find the first group that has a parent in which the user has the allowed roles.
    foreach ($groups as $group) {
      $parent = og_get_entity_groups('node', $group);

      while(isset($parent['node'])) {
        $parent = $parent['node'];
        $parent = array_pop($parent);
        $roles = og_get_user_roles('node', $parent, $account->uid);

        if(count(array_intersect(array_unique($roles), $allowedRoles)) > 0) {
          return TRUE;
        }

        $parent = og_get_entity_groups('node', $parent);
      }
    }

    return FALSE;
  }

  /**
   * Returns all groups a user can insert content into.
   *
   * @param $account
   * @param $group_type
   *
   * @return array|void
   */
  private function getGroupsByUser($account = NULL, $group_type = 'node') {
    $cache = &drupal_static(__FUNCTION__, array());

    $uid = 0;
    if ($account) {
      $uid = $account->uid;
    }

    if (isset($cache[$uid])) {
      return $cache[$uid];
    }

    $user_groups = og_get_entity_groups('user', $account);

    if ($user_groups) {
      $group_children = array();
      foreach ($user_groups[$group_type] as $user_group) {
        $group_children[$user_group] = $user_group;
        $group_children = array_merge($group_children, $this->getGroupChildren($user_group));
      }

      $cache[$uid] = $group_children;

      return $cache[$uid];
    }
    else {
      return array();
    }
  }

  /**
   * Returns all children of a group.
   *
   * @param $group
   *
   * @return array
   */
  private function getGroupChildren($group) {
    // TODO 'topic' should be loaded from workflow.yaml
    $group_bundle = 'topic';

    $query = db_select('og_membership', 'm')
      ->fields('m', array('id', 'etid', 'gid'))
      ->condition('gid', $group)
      ->condition('entity_type', 'node');

    $query->leftJoin('node', 'n', 'n.nid = m.etid');
    $query->condition('n.type', $group_bundle);

    $etids = array_keys($query->execute()
      ->fetchAllAssoc('etid'));

    if (count($etids)) {
      $children = array();
      foreach ($etids as $etid) {
        $children = array_merge($children, $this->getGroupChildren($etid));
      }

      return array_merge($etids, $children);
    }
    else {
      return $etids;
    }
  }
}
