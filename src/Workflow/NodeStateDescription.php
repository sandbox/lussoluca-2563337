<?php

namespace Drupal\rwf\Workflow;

/**
 * Builds full state descriptions of the most relevant versions of a node
 *
 * @package Drupal\rwf\Workflow
 */
class NodeStateDescription {

  CONST SCHEDULING_PREFIX = 's:';

  private $notYetPublishedState;

  private $alreadyPublishedState;

  function __construct($notYetPublishedState, $alreadyPublishedState) {
    $this->notYetPublishedState = $notYetPublishedState;
    $this->alreadyPublishedState = $alreadyPublishedState;
  }

  /**
   * Returns the description
   *
   * @return string
   */
  public function __toString() {
    if ($this->notYetPublishedState === NULL && $this->alreadyPublishedState === NULL) {
      return '';
    }

    if ($this->notYetPublishedState === NULL) {
      $description = $this->buildName($this->alreadyPublishedState);
    }
    elseif ($this->alreadyPublishedState === NULL) {
      $description = $this->buildName($this->notYetPublishedState);
    }
    else {
      $description = $this->buildName($this->alreadyPublishedState) .
        t(', con versione ') . $this->buildName($this->notYetPublishedState);
    }
    return $description;
  }

  private function buildName($machineName) {
    if (substr($machineName, 0, 2) === self::SCHEDULING_PREFIX) {
      $name = substr($machineName, 2) . t(' (P)');
    }
    else {
      $name = $machineName;
    }
    return $name;
  }

}
