<?php

namespace Drupal\rwf\Workflow;

use Wellnet\StateMachine\BaseGuard;

/**
 * A GuardInterface implementation that checks if a user is allowed to manage a
 * node depending on some roles.
 *
 * @see Transition
 */
abstract class RoleGuard extends BaseGuard {

  protected $allowedRoles;

  /**
   *
   * @param null $args
   *   associative array whose 'allowedRoles' key maps to an array that contains
   *   the names of allowed roles
   *
   */
  function __construct($args = NULL) {
    $this->allowedRoles = isset($args['allowedRoles']) ?
      $args['allowedRoles'] : array();
  }

  /**
   * Check if the user is an administrator
   *
   * @param array $context
   * @return bool
   * @throws \Exception if $context doesn't contain 'node' and 'user' keys
   */
  protected function basicCheck($context = array()) {

    // check if the context contains all needed information
    if (!isset($context['node']) || !isset($context['user'])) {
      throw new \Exception('The context does not contain all needed information');
    }

    // the administrator is always allowed
    return in_array('administrator', $context['user']->roles);
  }

}
