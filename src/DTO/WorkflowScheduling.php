<?php

namespace Drupal\rwf\DTO;


class WorkflowScheduling {

  /**
   * @var int
   */
  public $sid;

  /**
   * @var int
   */
  public $wid;

  /**
   * @var int
   */
  public $nid;

  /**
   * @var string
   */
  public $state;

  /**
   * @var int
   */
  public $scheduling_time;

}
