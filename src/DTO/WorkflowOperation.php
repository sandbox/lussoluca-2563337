<?php

namespace Drupal\rwf\DTO;


class WorkflowOperation {

  /**
   * @var int
   */
  public $lid;

  /**
   * @var int
   */
  public $wid;

  /**
   * @var int
   */
  public $nid;

  /**
   * @var string
   */
  public $event_type;

  /**
   * @var string
   */
  public $input;

  /**
   * @var int
   */
  public $old_vid;

  /**
   * @var int
   */
  public $vid;

  /**
   * @var string
   */
  public $old_state;

  /**
   * @var string
   */
  public $state;

  /**
   * @var int
   */
  public $forked_from;

  /**
   * @var string
   */
  public $comment;

  /**
   * @var int
   */
  public $scheduling_time;

  /**
   * @var int
   */
  public $changed;

  /**
   * @var int
   */
  public $uid;

}
