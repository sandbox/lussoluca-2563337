<?php

namespace Drupal\rwf\DTO;


class DTOUtils {

  public static function fillWorkflowOperation(WorkflowOperation $workflowOperation, Workflow $workflow) {
    $workflowOperation->nid = $workflow->nid;
    $workflowOperation->vid = $workflow->vid;
    $workflowOperation->state = $workflow->state;
    $workflowOperation->changed = REQUEST_TIME;
    $workflowOperation->uid = $GLOBALS['user']->uid;
  }

}
