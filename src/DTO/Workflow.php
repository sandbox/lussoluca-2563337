<?php

namespace Drupal\rwf\DTO;


class Workflow {

  /**
   * @var int
   */
  public $wid;

  /**
   * @var int
   */
  public $nid;

  /**
   * @var int
   */
  public $vid;

  /**
   * @var string
   */
  public $state;

  /**
   * @var int
   */
  public $changed;

}
