<?php

namespace Drupal\rwf;

use Drupal\rwf\DAO\WorkflowDAO;
use Drupal\rwf\DAO\WorkflowOperationDAO;
use Drupal\rwf\DAO\WorkflowSchedulingDAO;
use Drupal\rwf\StateMachine\VersionsManager;
use Drupal\rwf\StateMachine\WorkflowManager;
use Drupal\rwf\Workflow\ScheduledTransitionExecutor;
use Wellnet\StateMachine\StateMachine;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class RwfServiceProvider
 */
class RwfServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(Container $pimple) {
    $pimple['rwf.event_dispatcher'] = function ($c) {
      return new EventDispatcher();
    };
    $pimple['rwf.state_machine_config'] = function ($c) {
      return file_get_contents(variable_get('rwf_workflow_config_file'));
    };
    $pimple['rwf.configurator'] = function ($c) {
      return new YamlConfigurator($c['rwf.state_machine_config']);
    };
    $pimple['rwf.state_machine'] = function ($c) {
      return new StateMachine($c, $c['rwf.event_dispatcher'], $c['rwf.configurator']);
    };
    $pimple['rwf.workflow_dao'] = function ($c) {
      return new WorkflowDAO();
    };
    $pimple['rwf.workflow_scheduling_dao'] = function ($c) {
      return new WorkflowSchedulingDAO();
    };
    $pimple['rwf.workflow_operation_dao'] = function ($c) {
      return new WorkflowOperationDAO();
    };
    $pimple['rwf.versions_manager'] = function ($c) {
      return new VersionsManager($c['rwf.workflow_dao'], $c['rwf.workflow_operation_dao'], $c['rwf.workflow_scheduling_dao'], $c['rwf.event_dispatcher']);
    };
    $pimple['rwf.workflow_manager'] = function ($c) {
      return new WorkflowManager($c['rwf.state_machine'], $c['rwf.workflow_dao'], $c['rwf.workflow_scheduling_dao'], $c['rwf.workflow_operation_dao'], $c['rwf.versions_manager']);
    };
    $pimple['rwf.scheduled_transitions_executor'] = function ($c) {
      return new ScheduledTransitionExecutor($c['rwf.workflow_dao'], $c['rwf.workflow_scheduling_dao'], $c['rwf.workflow_operation_dao'], $c['rwf.versions_manager']);
    };
  }
}
