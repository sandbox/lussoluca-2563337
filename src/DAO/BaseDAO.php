<?php

namespace Drupal\rwf\DAO;


/**
 * Base for the simple abstraction layer used to interact with rwf
 * tables.
 *
 * Each DAO implementation interacts with a single table and is bound to a DTO
 * class to map single rows.
 *
 * @package Drupal\rwf\DAO
 */
abstract class BaseDAO {

  /** @var string */
  protected $table;

  /** @var string */
  protected $dtoClass;

  public function fieldsFromDtos($dto) {
    $fields = array();
    foreach ($dto as $key => $value) {
      if (isset($value)) {
        $fields[$key] = $value;
      }
    }
    return $fields;
  }

  public function fillDtos($results) {
    $dtos = array();

    foreach ($results as $row) {
      $dto = (new \ReflectionClass($this->dtoClass))->newInstance();
      foreach ($row as $key => $value) {
        if (isset($value)) {
          $dto->$key = $value;
        }
      }
      $dtos[] = $dto;
    }

    return $dtos;
  }

  /**
   * Removes all contents related to the given $nid.
   *
   * @param $nid
   */
  public function deleteByNid($nid) {
    db_delete($this->table)
      ->condition('nid', $nid)
      ->execute();
  }

}
