<?php

namespace Drupal\rwf\DAO;

use Drupal\rwf\DTO\Workflow;

/**
 * DAO for the 'rwf_workflow' table.
 *
 * @package Drupal\rwf\DAO
 *
 * @see Workflow
 */
class WorkflowDAO extends BaseDAO {

  /**
   *
   */
  function __construct() {
    $this->table = 'rwf_workflow';
    $this->dtoClass = '\Drupal\rwf\DTO\Workflow';
  }

  /**
   * @param \Drupal\rwf\DTO\Workflow $workflow
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function saveOrUpdate(Workflow $workflow) {

    $fields = $this->fieldsFromDtos($workflow);

    db_merge($this->table)
      ->key(array('wid' => $workflow->wid))
      ->fields($fields)
      ->execute();

    if (!isset($workflow->wid)) {
      $workflow->wid = $this->findByVid($workflow->vid)->wid;
    }

  }

  /**
   * @param $wid
   *
   * @return mixed
   */
  public function findByWid($wid) {
    $example = new Workflow();
    $example->wid = $wid;
    $results = $this->findByExample($example);
    return empty($results) ? NULL : $results[0];
  }

  /**
   * @param $vid
   *
   * @return null
   */
  public function findByVid($vid) {
    $example = new Workflow();
    $example->vid = $vid;
    $results = $this->findByExample($example);
    return empty($results) ? NULL : $results[0];
  }

  /**
   * @param $nid
   *
   * @return array
   */
  public function findByNid($nid) {
    $example = new Workflow();
    $example->nid = $nid;
    return $this->findByExample($example);
  }

  /**
   * @param Workflow $exampleWorkflow
   * @return array
   */
  private function findByExample(Workflow $exampleWorkflow) {

    $query = db_select($this->table)
      ->fields($this->table);
    foreach ($exampleWorkflow as $key => $value) {
      if (isset($value)) {
        $query->condition($key, $value);
      }
    }
    $results = $query
      ->orderBy('wid', 'DESC')
      ->execute();

    return $this->fillDtos($results, 'Workflow');
  }

  /**
   * @param $nid
   * @param array $states
   *
   * @return array
   */
  public function getVersionsByState($nid, array $states) {

    $results = db_select($this->table)
      ->fields($this->table)
      ->condition('nid', $nid)
      ->condition('state', $states)
      ->execute();

    return $this->fillDtos($results);
  }

  /**
   * Updates the state of all workflows on the given $nid except for those
   * related to the specified $vid.
   *
   * The query does
   *
   * @param $nid
   *   nid of the workflows to update
   * @param $vid
   *   vid of the workflow to exclude from state update
   * @param $state
   *   The new state
   * @return array
   *   The list of actually updated workflows
   */
  public function unpublishPreviousVersions($nid, $vid, $state) {

    // retrieve the rows that will actually be updated
    $results = db_select($this->table)
      ->fields($this->table)
      ->condition('nid', $nid)
      ->condition('vid', $vid, '<>')
      ->condition('state', $state, '<>')
      ->execute();

    db_update($this->table)
      ->fields(array(
        'state' => $state,
      ))
      ->condition('nid', $nid)
      ->condition('vid', $vid, '<>')
      ->execute();

    return $this->fillDtos($results);
  }
}
