<?php

namespace Drupal\rwf\DAO;


use Drupal\rwf\DTO\WorkflowOperation;

/**
 * DAO for the 'rwf_operation' table.
 *
 * @package Drupal\rwf\DAO
 *
 * @see WorkflowOperation
 */
class WorkflowOperationDAO extends BaseDAO {

  function __construct() {
    $this->table = 'rwf_operation';
    $this->dtoClass = '\Drupal\rwf\DTO\WorkflowOperation';
  }

  public function save(WorkflowOperation $workflowOperation) {

    $fields = $this->fieldsFromDtos($workflowOperation);

    db_insert($this->table)
      ->fields($fields)
      ->execute();
  }

  public function findByLid($lid) {
    $example = new WorkflowOperation();
    $example->lid = $lid;
    return $this->findByExample($example)[0];
  }

  public function findByWid($wid) {
    $example = new WorkflowOperation();
    $example->wid = $wid;
    return $this->findByExample($example);
  }

  public function findByVid($vid) {
    $example = new WorkflowOperation();
    $example->vid = $vid;
    return $this->findByExample($example);
  }

  /**
   * @param WorkflowOperation $exampleWorkflowLog
   * @return array
   */
  private function findByExample(WorkflowOperation $exampleWorkflowLog) {

    $query = db_select($this->table)
      ->fields($this->table);
    foreach ($exampleWorkflowLog as $key => $value) {
      if (isset($value)) {
        $query->condition($key, $value);
      }
    }
    $results = $query
      ->orderBy('lid', 'DESC')
      ->execute();

    return $this->fillDtos($results, 'WorkflowOperation');
  }

}
