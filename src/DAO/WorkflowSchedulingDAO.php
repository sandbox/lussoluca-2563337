<?php

namespace Drupal\rwf\DAO;


use Drupal\rwf\DTO\WorkflowScheduling;

/**
 * DAO for the 'rwf_scheduling' table.
 *
 * @package Drupal\rwf\DAO
 *
 * @see WorkflowScheduling
 */
class WorkflowSchedulingDAO extends BaseDAO {

  function __construct() {
    $this->table = 'rwf_scheduling';
    $this->dtoClass = '\Drupal\rwf\DTO\WorkflowScheduling';
  }

  public function saveScheduling(WorkflowScheduling $workflowScheduling) {

    $fields = $this->fieldsFromDtos($workflowScheduling);

    $sid = db_insert($this->table)
      ->fields($fields)
      ->execute();
    $workflowScheduling->sid = $sid;
  }

  public function deleteScheduling(WorkflowScheduling $workflowScheduling) {
    db_delete($this->table)
      ->condition('sid', $workflowScheduling->sid)
      ->execute();
  }

  public function deleteSchedulingByWid($wid) {
    db_delete($this->table)
      ->condition('wid', $wid)
      ->execute();
  }

  public function getSchedulingByWid($wid) {
    $results = db_select($this->table)
      ->fields($this->table)
      ->condition('wid', $wid)
      ->orderBy('scheduling_time', 'DESC')
      ->execute();

    $scheduling = array();

    foreach ($results as $row) {
      $ws = new WorkflowScheduling();
      foreach ($row as $key => $value) {
        if (isset($value)) {
          $ws->$key = $value;
        }
      }
      $scheduling[] = $ws;
    }

    return $scheduling;
  }


  /**
   * Returns all transitions whose scheduling time is less than this request
   * time.
   *
   * @return array
   *   array of WorkflowOperation instances
   */
  public function getAllSchedules() {
    $results = db_select($this->table)
      ->fields($this->table)
      ->condition('scheduling_time', REQUEST_TIME, '<')
      ->execute();
    return $this->fillDtos($results, 'WorkflowScheduling');
  }

}