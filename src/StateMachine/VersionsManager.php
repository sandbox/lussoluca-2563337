<?php

namespace Drupal\rwf\StateMachine;


use Drupal\rwf\DAO\WorkflowDAO;
use Drupal\rwf\DAO\WorkflowOperationDAO;
use Drupal\rwf\DAO\WorkflowSchedulingDAO;
use Drupal\rwf\DTO\WorkflowOperation;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * It's responsible of executing all workflow operations at node level.
 *
 * @package Drupal\rwf\StateMachine
 */
class VersionsManager {

  const COLLATERAL_EXECUTED = 'version-manager.collateral_executed';

  /** @var WorkflowDAO */
  private $workflowDAO;

  /** @var WorkflowOperationDAO */
  private $workflowOperationDAO;

  /** @var WorkflowSchedulingDAO */
  private $workflowSchedulingDAO;

  /** @var EventDispatcher */
  private $eventDispatcher;

  /**
   * @param \Drupal\rwf\DAO\WorkflowDAO $workflowDAO
   * @param \Drupal\rwf\DAO\WorkflowOperationDAO $workflowOperationDAO
   * @param \Drupal\rwf\DAO\WorkflowSchedulingDAO $workflowSchedulingDAO
   * @param \Symfony\Component\EventDispatcher\EventDispatcher $eventDispatcher
   */
  public function __construct(WorkflowDAO $workflowDAO, WorkflowOperationDAO $workflowOperationDAO, WorkflowSchedulingDAO $workflowSchedulingDAO, EventDispatcher $eventDispatcher) {
    $this->workflowDAO = $workflowDAO;
    $this->workflowOperationDAO = $workflowOperationDAO;
    $this->workflowSchedulingDAO = $workflowSchedulingDAO;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * @param $node
   *
   * @return array
   */
  public function getNodeVersions($node) {
    return $this->workflowDAO->findByNid($node->nid);
  }

  /**
   * Delete everything related to the given $node.
   *
   * @param $node
   */
  public function deleteNodeVersions($node) {
    $this->workflowDAO->deleteByNid($node->nid);
    $this->workflowOperationDAO->deleteByNid($node->nid);
    $this->workflowSchedulingDAO->deleteByNid($node->nid);
  }

  /**
   * Checks if the node is in a state that allows to create a new version.
   *
   * @param $node
   * @return bool
   */
  public function isNodeForkable($node) {
    $unpublishedStates = explode(',', variable_get('rwf_workflow_unpublished_states'));
    $notYetLiveVersions = $this->workflowDAO->getVersionsByState($node->nid, $unpublishedStates);
    return empty($notYetLiveVersions);
  }

  /**
   * Retrieved the public version of the given $node.
   *
   * @param $node
   * @return null
   */
  public function getPublicVersion($node) {
    $publicStates = explode(',', variable_get('rwf_workflow_public_states'));
    $publicVersion = $this->workflowDAO->getVersionsByState($node->nid, $publicStates);
    return empty($publicVersion) ? NULL : $publicVersion[0];
  }

  /**
   * Execute collateral operations related to the new state of a version.
   *
   * If the version is published, set the 'status' option and move previous versions
   * to the unpublished state.
   *
   * If the node is archived, raise the 'archived' flag
   *
   * If the node is unpublished, remove the 'status' option.
   *
   * @param $nid
   * @param $vid
   * @param $state
   * @throws \Exception
   */
  public function executeCollateralOperations($nid, $vid, $state) {

    $flag = flag_get_flag('archived');
    $node = node_load($nid);
    $admin = user_load(1);

    // publish the version
    if ($state === variable_get('rwf_workflow_published_state')) {

      // if this is the first version or the previous ones are unpublished
      if ($node->status == NODE_NOT_PUBLISHED) {
        // publish the node
        $node->status = NODE_PUBLISHED;
        node_save($node);
      }

      // otherwise unpublish all previous versions and/or remove the archived flag
      else {
        $unpublishedState = variable_get('rwf_workflow_old_version_state');
        $unpublishedVersions = $this->workflowDAO->unpublishPreviousVersions($nid, $vid, $unpublishedState);

        // log the operation for each unpublished version
        $unpublish = new WorkflowOperation();
        $unpublish->event_type = WorkflowManager::EVENT_UNPUBLISH_OLD_VERSIONS;
        $unpublish->nid = $nid;
        $unpublish->state = $unpublishedState;
        $unpublish->changed = REQUEST_TIME;
        foreach ($unpublishedVersions as $unpublishedVersion) {
          $unpublish->wid = $unpublishedVersion->wid;
          $unpublish->vid = $unpublishedVersion->vid;
          $this->workflowOperationDAO->save($unpublish);
        }

        $flag_status = $flag->flag('unflag', $nid, $admin);
        if ($flag_status) {
          watchdog('rwf', "Un-archive node {$nid}");
        }
      }

      $this->clearCachePage($node);
    }

    // archive the version
    elseif ($state === variable_get('rwf_workflow_archived_state')) {
      $flag_status = $flag->flag('flag', $nid, $admin);
      if ($flag_status) {
        watchdog('rwf', "Archive node {$nid}");
        $this->clearCachePage($node);
      }
    }

    // unpublish the version and/or remove the archived flag
    elseif ($state === variable_get('rwf_workflow_old_version_state')) {
      // unpublish the node
      $node->status = NODE_NOT_PUBLISHED;
      node_save($node);

      $flag_status = $flag->flag('unflag', $nid, $admin);
      if ($flag_status) {
        watchdog('rwf', "Un-archive node {$nid}");
      }

      $this->clearCachePage($node);
    }

    $this->eventDispatcher->dispatch(VersionsManager::COLLATERAL_EXECUTED, new CollateralExecutedEvent($nid, $vid, $state));
  }

  /**
   * Clear the internal cache_page and notify external caches.
   *
   * @param $node
   */
  private function clearCachePage($node) {
    cache_clear_all('*', 'cache_page', TRUE);

    if (module_exists('expire')) {
      expire_execute_expiration('node', $node, EXPIRE_NODE_UPDATE);
    }
  }
}
