<?php

namespace Drupal\rwf\StateMachine;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class CollateralExecutedEvent
 */
class CollateralExecutedEvent extends Event {

  /**
   * @var
   */
  private $nid;

  /**
   * @var
   */
  private $vid;

  /**
   * @var
   */
  private $state;

  /**
   * @param $nid
   * @param $vid
   * @param $state
   */
  public function __construct($nid, $vid, $state) {
    $this->nid = $nid;
    $this->vid = $vid;
    $this->state = $state;
  }

  /**
   * @return mixed
   */
  public function getNid() {
    return $this->nid;
  }

  /**
   * @return mixed
   */
  public function getVid() {
    return $this->vid;
  }

  /**
   * @return mixed
   */
  public function getState() {
    return $this->state;
  }
}
