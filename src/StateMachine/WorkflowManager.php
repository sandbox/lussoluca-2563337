<?php

namespace Drupal\rwf\StateMachine;


use Drupal\rwf\DAO\WorkflowDAO;
use Drupal\rwf\DAO\WorkflowOperationDAO;
use Drupal\rwf\DAO\WorkflowSchedulingDAO;
use Drupal\rwf\DTO\DTOUtils;
use Drupal\rwf\DTO\Workflow;
use Drupal\rwf\DTO\WorkflowOperation;
use Drupal\rwf\DTO\WorkflowScheduling;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Wellnet\StateMachine\InputEvent;
use Wellnet\StateMachine\State;
use Wellnet\StateMachine\StateMachine;
use Wellnet\StateMachine\StateMachineEvents;
use Wellnet\StateMachine\Transition;
use Wellnet\StateMachine\TransitionEvent;

/**
 * It's responsible of executing operations at version level.
 *
 * An instance manages a workflow and can be initialized from
 * the node object or from the wid identifier.
 *
 * It leverages a StateMachine instance to perform steps in the workflow.
 *
 * @package Drupal\rwf\StateMachine
 * @see StateMachine
 * @see Workflow
 */
class WorkflowManager {

  CONST EVENT_TRANSITION_UNSCHEDULED = 'workflow-manager.transition.unscheduled';
  CONST EVENT_UNPUBLISH_OLD_VERSIONS = 'workflow-manager.old-versions.unpublish';

  /** @var EventDispatcher */
  private $eventDispatcher;

  /** @var StateMachine */
  private $stateMachine;

  /** @var WorkflowDAO */
  private $workflowDAO;

  /** @var WorkflowSchedulingDAO */
  private $workflowSchedulingDAO;

  /** @var WorkflowOperationDAO */
  private $workflowOperationDAO;

  /** @var Workflow */
  private $workflow;

  private $node;

  /** @var string */
  private $comment;

  /** @var int */
  private $forked_from;

  /** @var VersionsManager */
  private $versionsManager;

  public function __construct(StateMachine $stateMachine, WorkflowDAO $workflowDAO, WorkflowSchedulingDAO $workflowSchedulingDAO, WorkflowOperationDAO $workflowOperationDAO, VersionsManager $versionsManager) {
    $this->stateMachine = $stateMachine;
    $this->eventDispatcher = $this->stateMachine->getEventDispatcher();
    $this->workflowDAO = $workflowDAO;
    $this->workflowSchedulingDAO = $workflowSchedulingDAO;
    $this->workflowOperationDAO = $workflowOperationDAO;
    $this->versionsManager = $versionsManager;
  }

  /**
   * Initialize $this instance from a $node.
   *
   * @param $node
   * @throws \Exception
   *   If $this instance has already been initialized with a different $node
   */
  public function initFromNode($node) {
    // prevent multiple initializations
    if (isset($this->workflow)) {
      if ($this->node->nid != $node->nid) {
        throw new \Exception('$this WorkflowManager has already been initialized with another node revision');
      }
    }
    else {
      $this->node = $node;
      $this->workflow = $this->workflowDAO->findByVid($node->vid);
    }
  }

  /**
   * Updates the node object of $this instance
   *
   * @param $node
   * @throws \Exception
   *   If $this instance has already been initialized with a different $node
   */
  public function updateNode($node) {
    if ($this->node->nid != $node->nid) {
      throw new \Exception('$this WorkflowManager has already been initialized with another node');
    }

    $this->node = $node;
  }

  /**
   * Initialize $this instance from a $wid.
   * @param $wid
   * @throws \Exception
   *   If $this instance has already been initialized with a different $wid
   */
  public function initFromWid($wid) {
    // prevent multiple initializations
    if (isset($this->workflow)) {
      if ($this->workflow->wid != $wid) {
        throw new \Exception('$this WorkflowManager has already been initialized with another workflow');
      }
    }
    else {
      $this->workflow = $this->workflowDAO->findByWid($wid);
      $this->node = node_load(NULL, $this->workflow->vid);
    }
  }

  public function getWorkflow() {
    return $this->workflow;
  }

  public function getNode() {
    return $this->node;
  }

  /**
   * Creates a new version. If $forkedFrom is specified, it's a fork of a
   * previous version.
   * @param null $forkedFrom
   *   WID of the previous version
   */
  public function createVersion($forkedFrom = NULL) {

    $this->comment = $this->node->revision_comment;
    $this->forked_from = $forkedFrom;
    $this->eventDispatcher->addListener(StateMachineEvents::START, array(
      $this,
      'onStart'
    ));
    $this->stateMachine->start();
  }

  /**
   * Returns TRUE if this version is editable.
   *
   * A version is editable if:
   * - it is in one of the states configured as editable
   * - there isn't a scheduled transition
   *
   * @return bool
   */
  public function isVersionEditable() {
    $isEditable = FALSE;

    $currentState = $this->workflow->state;
    $editableStates = explode(',', variable_get('rwf_workflow_editable_states'));
    if (in_array($currentState, $editableStates)) {
      $scheduling = $this->getSchedules();
      $isEditable = empty($scheduling);
    }

    return $isEditable;
  }

  /**
   *  Creates a new revision.
   *
   * @see rwf_node_update
   */
  public function createRevision() {
    $this->workflow = $this->workflowDAO->findByVid($this->node->old_vid);
    $input = variable_get('rwf_node_update_input');

    $this->sendInput($input, $this->node->revision_comment);
  }

  /**
   * Sends an input to the state machine or, if $schedulingTime is set,
   * schedules its execution.
   *
   * @param $input
   * @param string $comment
   * @param null $schedulingTime
   */
  public function executeTransition($input, $comment = '', $schedulingTime = NULL) {

    // schedule the execution
    if (isset($schedulingTime)) {
      // create scheduling
      $workflowScheduling = new WorkflowScheduling();
      $workflowScheduling->wid = $this->workflow->wid;
      $workflowScheduling->nid = $this->workflow->nid;
      /** @var State $state */
      $state = $this->getAllowedInputs()[$input];
      $workflowScheduling->state = $state->getName();
      $workflowScheduling->scheduling_time = $schedulingTime ;
      $this->workflowSchedulingDAO->saveScheduling($workflowScheduling);

      // log operation
      $workflowOperation = new WorkflowOperation();
      $workflowOperation->wid = $this->workflow->wid;
      $workflowOperation->event_type = 'workflow-manager.transition.scheduled';
      $workflowOperation->input = $input;
      $workflowOperation->comment = $comment;
      $workflowOperation->scheduling_time = $schedulingTime;
      $this->log($workflowOperation);

    }

    // execute the transition immediately
    else {
      $this->sendInput($input, $comment);
    }

  }

  /**
   * Delete all schedules related to the managed workflow
   *
   * @param string $comment
   */
  public function deleteScheduling($comment = '') {

    $this->workflowSchedulingDAO->deleteSchedulingbyWid($this->workflow->wid);

    $workflowOperation = new WorkflowOperation();
    $workflowOperation->event_type = self::EVENT_TRANSITION_UNSCHEDULED;
    $workflowOperation->wid = $this->workflow->wid;
    $workflowOperation->comment = $comment;
    $this->log($workflowOperation);
  }

  /**
   * Returns all schedules related to the managed workflow.
   *
   * @return array
   *   of WorkflowScheduling instances
   */
  public function getSchedules() {
    return $this->workflowSchedulingDAO->getSchedulingByWid($this->workflow->wid);
  }

  /**
   * Helper method to send an input to the state machine.
   *
   * @param $input
   *   The input to send
   * @param $comment
   *   The comment for the transition that could derive from this input
   */
  private function sendInput($input, $comment) {
    $this->comment = $comment;
    $this->eventDispatcher->addListener(StateMachineEvents::TRANSITION_SUCCEEDED, array(
      $this,
      'onTransition'
    ));

    $this->resumeStateMachine();
    $context = array(
      'node' => $this->node,
      'user' => $GLOBALS['user'],
    );
    $this->eventDispatcher->dispatch(StateMachineEvents::INPUT, new InputEvent($input, $context));
  }

  /**
   * Returns all the valid inputs for the current states.
   *
   * @return array
   *   Keys are inputs, values are destinations.
   */
  public function getAllowedInputs() {
    $allowedInputs = array();

    $this->resumeStateMachine();
    $context = array(
      'node' => $this->node,
      'user' => $GLOBALS['user'],
    );
    $allowedTransitions = $this->stateMachine->getAllowedTransitions($context);
    /** @var Transition $transition */
    foreach ($allowedTransitions as $transition) {
      $allowedInputs[$transition->getInput()] = $transition->getDestination();
    }

    return $allowedInputs;
  }

  /**
   * Helper method to resume the state machine with the current state.
   *
   * @see StateMachine
   */
  private function resumeStateMachine() {
    if (!$this->stateMachine->isRunning()) {
      $this->stateMachine->resume($this->workflow->state);
    }
  }

  /**
   * Returns all operations executed on the managed workflow.
   *
   * @return array
   */
  public function getAllOperations() {
    return $this->workflowOperationDAO->findByWid($this->workflow->wid);
  }

  /**
   * Returns all information related to the given operation.
   *
   * @param $lid
   * @return array
   */
  public function getOperationInfo($lid) {
    /** @var WorkflowOperation $workflowOperation*/
    $workflowOperation = $this->workflowOperationDAO->findByLid($lid);
    return array(
      'node' => node_load(NULL, $workflowOperation->vid),
      'timestamp' => $workflowOperation->changed,
    );
  }

  /**
   * Handler for the state machine start event.
   *
   * @param Event $event
   * @param $eventName
   */
  public function onStart(Event $event, $eventName) {

    $this->node->status = NODE_NOT_PUBLISHED;
    $this->node->promote = NODE_NOT_PROMOTED;

    $this->workflow = new Workflow();
    $this->workflow->nid = $this->node->nid;
    $this->workflow->vid = $this->node->vid;
    $this->workflow->state = $this->stateMachine->getCurrentState()->getName();
    $this->workflow->changed = REQUEST_TIME;

    $this->workflowDAO->saveOrUpdate($this->workflow);

    $workflowOperation = new WorkflowOperation();
    $workflowOperation->event_type = $eventName;
    $workflowOperation->comment = $this->comment;
    $workflowOperation->wid = $this->workflow->wid;
    $workflowOperation->forked_from = $this->forked_from;

    $this->log($workflowOperation);
  }

  /**
   * Handler for the state machine transition event.
   *
   * According to the event, it updates the state of the managed workflow.
   * If this version is being published, unpublish all previous versions.
   *
   * @param TransitionEvent $event
   * @param $eventName
   */
  public function onTransition(TransitionEvent $event, $eventName) {

    // update the Workflow DTO
    $this->workflow->state = $event->getTransition()
      ->getDestination()
      ->getName();
    // update vid in case of a new revision
    $this->workflow->vid = $this->node->vid;
    $this->workflow->changed = REQUEST_TIME;
    $this->workflowDAO->saveOrUpdate($this->workflow);

    $this->versionsManager->executeCollateralOperations($this->node->nid, $this->node->vid, $this->workflow->state);

    // log the operation
    $workflowOperation = new WorkflowOperation();
    $workflowOperation->event_type = $eventName;
    $workflowOperation->input = $event->getTransition()->getInput();
    if (isset ($this->node->old_vid)) {
      $workflowOperation->old_vid = $this->node->old_vid;
    }
    $workflowOperation->old_state = $event->getTransition()
      ->getSource()
      ->getName();
    $workflowOperation->comment = $this->comment;
    $workflowOperation->wid = $this->workflow->wid;
    $this->log($workflowOperation);
  }


  private function log(WorkflowOperation $workflowOperation) {
    DTOUtils::fillWorkflowOperation($workflowOperation, $this->workflow);
    $this->workflowOperationDAO->save($workflowOperation);
  }

}
