<?php

namespace Drupal\rwf;

use Symfony\Component\Yaml\Yaml;
use Wellnet\StateMachine\ConfiguratorInterface;

/**
 * Class YamlConfigurator
 */
class YamlConfigurator implements ConfiguratorInterface {

  /**
   * @var array
   */
  private $config;

  /**
   * Please check the test.yml file to see the proper structure of the
   * configuration file.
   *
   * @param string $file
   */
  public function __construct($file) {
    $this->config = Yaml::parse($file);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig() {
    return $this->config;
  }
}
