<?php

/**
 * Implements hook_drush_command().
 */
function rwf_drush_command() {
  $items['rwf-init'] = array(
    'description' => 'Initialize rwf for existing nodes.',
    'options' => array(
      'drupal-status' => 'Node status.',
      'rwf-status' => 'Workflow status to set.',
      'types' => 'Comma separated list of content type to act on.',
    ),
    'aliases' => array('ri'),
  );

  return $items;
}

/**
 *
 */
function drush_rwf_init() {
  $drupal_status = drush_get_option('drupal-status');
  $rwf_status = drush_get_option('rwf-status');
  $types = drush_get_option('types');

  if ($drupal_status != 'published' && $drupal_status != 'unpublished') {
    drush_log(dt('Invalid drupal-status'), 'error');
  }

  $nids = db_select('node', 'n')
    ->fields('n', array('nid', 'vid', 'type'))
    ->condition('type', explode(',', $types))
    ->condition('status', ('published' == $drupal_status) ? 1 : 0)
    ->execute()
    ->fetchAllAssoc('nid');

  foreach ($nids as $nid => $data) {
    $vid = $data->vid;
    $type = $data->type;

    $wid = db_insert('rwf_workflow')
      ->fields(array('nid', 'vid', 'state', 'changed'), array(
        $nid,
        $vid,
        $rwf_status,
        time()
      ))
      ->execute();

    db_insert('rwf_operation')
      ->fields(array(
        'wid',
        'nid',
        'event_type',
        'input',
        'vid',
        'state',
        'changed',
        'uid'
      ), array(
        $wid,
        $nid,
        'state-machine.transition.succeeded',
        $rwf_status,
        $vid,
        $rwf_status,
        time(),
        1
      ))
      ->execute();

    drush_log("Processed node {$type} -> {$nid}", 'ok');
  }


  drush_log($drupal_status, 'ok');
  drush_log($rwf_status, 'ok');
}
