<?php

/**
 * @file
 * Administrative settings for the Revision Workflow module.
 */

/**
 *
 * Displays the content type admin overview page.
 *
 * The admin can select the content types that will leverage rwf features
 *
 */
function rwf_content_types_page($form, &$form_state) {

  // display a checkbox for each content type
  $types = node_type_get_types();
  foreach ($types as $node_type) {
    $form['rwf_enable_' . $node_type->type] = array(
      '#title' => $node_type->name,
      '#type' => 'checkbox',
      '#default_value' => variable_get('rwf_enable_' . $node_type->type, 0),
    );
  }

  $form['#submit'][] = '_set_node_type_options';

  return system_settings_form($form);
}

/**
 * Set default "Publishing options" for all content types managed by rwf
 *
 * @param $form
 * @param $form_state
 */
function _set_node_type_options($form, &$form_state) {
  foreach ($wid = $form_state['values'] as $key => $value) {
    if (strpos($key, 'rwf_enable_') === 0) {
      if ($value) {
        $nodeType = str_replace('rwf_enable_', '', $key);

        // set default options
        variable_set('node_options_' . $nodeType, array('revision'));

        // TODO FASE 2: create the workflow for existing nodes
        /*
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', $nodeType);
        $results = $query->execute();
        if(!empty($results['node'])) {
          // TODO consider loading nodes one at a time to avoid memory issues
          $nids = array_keys($results['node']);
          $nodes = node_load_multiple($nids);
          foreach($nodes as $node) {
            // TODO FASE 2: create workflow and operation
          }
        }
        */

      }
    }
  }
}

/**
 *
 * Displays settings page.
 */
function rwf_settings_form($form, &$form_state) {

  $form['rwf_workflow_config_file'] = array(
    '#title' => t('The workflow configuration file'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_workflow_config_file'),
  );

  $form['rwf_node_update_input'] = array(
    '#title' => t('Input triggered by node update'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_node_update_input'),
  );

  $form['rwf_workflow_unpublished_states'] = array(
    '#title' => t('Unpublished states'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_workflow_unpublished_states'),
    '#description' => t('Enter a comma-separated list of state names'),
  );

  $form['rwf_workflow_published_state'] = array(
    '#title' => t('Publishing state'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_workflow_published_state'),
  );

  $form['rwf_workflow_public_states'] = array(
    '#title' => t('Public states'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_workflow_public_states'),
    '#description' => t('Enter a comma-separated list of state names'),
  );

  $form['rwf_workflow_old_version_state'] = array(
    '#title' => t('State of old versions'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_workflow_old_version_state'),
  );

  $form['rwf_workflow_editable_states'] = array(
    '#title' => t('Editable states'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_workflow_editable_states'),
    '#description' => t('Enter a comma-separated list of state names'),
  );

  $form['rwf_workflow_archived_state'] = array(
    '#title' => t('Archived state'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rwf_workflow_archived_state'),
  );

  $form['#validate'] = array('rwf_settings_form_validate');

  return system_settings_form($form);
}

/**
 * Form validation handler for rwf_settings_form().
 */
function rwf_settings_form_validate($form, &$form_state) {

  $workflowConfigFile = $form_state['values']['rwf_workflow_config_file'];
  if (!file_exists($workflowConfigFile)) {
    form_set_error('rwf_workflow_config_file', t('Invalid path'));
  }

  // TODO ensure that all state names are available in workflow.yml

}
