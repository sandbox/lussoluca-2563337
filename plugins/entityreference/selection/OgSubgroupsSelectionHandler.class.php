<?php

/**
 * Class OgSubgroupsSelectionHandler
 */
class OgSubgroupsSelectionHandler extends OgSelectionHandler {

  /**
   * Overrides OgSelectionHandler::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return new OgSubgroupsSelectionHandler($field, $instance, $entity_type, $entity);
  }

  /**
   * Overrides OgSelectionHandler::buildEntityFieldQuery().
   */
  public function buildEntityFieldQuery($match = NULL, $match_operator = 'CONTAINS') {
    $group_type = $this->field['settings']['target_type'];
    if (empty($this->instance['field_mode']) || $group_type != 'node') {
      return parent::buildEntityFieldQuery($match, $match_operator);
    }

    $handler = EntityReference_SelectionHandler_Generic::getInstance($this->field, $this->instance, $this->entity_type, $this->entity);
    $query = $handler->buildEntityFieldQuery($match, $match_operator);

    // FIXME: http://drupal.org/node/1325628
    unset($query->tags['node_access']);

    // FIXME: drupal.org/node/1413108
    unset($query->tags['entityreference']);

    $query->addTag('entity_field_access');
    $query->addTag('og');

    $group_type = $this->field['settings']['target_type'];
    $entity_info = entity_get_info($group_type);

    if (!field_info_field(OG_GROUP_FIELD)) {
      // There are no groups, so falsify query.
      $query->propertyCondition($entity_info['entity keys']['id'], -1, '=');
      return $query;
    }

    // Show only the entities that are active groups.
    $query->fieldCondition(OG_GROUP_FIELD, 'value', 1, '=');

    if (empty($this->instance['field_mode'])) {
      return $query;
    }

    $user_groups = $this->getGroupsByUser(NULL, $group_type);
    $user_groups = $user_groups ? $user_groups : array();

    if ($user_groups) {
      $query->propertyCondition($entity_info['entity keys']['id'], $user_groups, 'IN');
    }
    else {
      // User doesn't have permission to select any group so falsify this
      // query.
      $query->propertyCondition($entity_info['entity keys']['id'], -1, '=');
    }

    return $query;
  }

  /**
   * Returns all groups a user can insert content into.
   *
   * @param $account
   * @param $group_type
   *
   * @return array|void
   */
  function getGroupsByUser($account = NULL, $group_type = 'node') {
    $cache = &drupal_static(__FUNCTION__, array());

    $uid = 0;
    if ($account) {
      $uid = $account->uid;
    }

    if (isset($cache[$uid])) {
      return $cache[$uid];
    }

    $user_groups = og_get_entity_groups('user', $account);

    if ($user_groups) {
      $group_children = array();
      foreach ($user_groups[$group_type] as $user_group) {
        $group_children[$user_group] = $user_group;
        $group_children = array_merge($group_children, $this->getGroupChildren($user_group));
      }

      $cache[$uid] = $group_children;

      return $cache[$uid];
    }
    else {
      return array();
    }
  }

  /**
   * Returns all children of a group.
   *
   * @param $group
   *
   * @return array
   */
  function getGroupChildren($group) {
    $group_bundle = variable_get('taxonomy_og_group_bundle', 'topic');

    $query = db_select('og_membership', 'm')
      ->fields('m', array('id', 'etid', 'gid'))
      ->condition('gid', $group)
      ->condition('entity_type', 'node');

    $query->leftJoin('node', 'n', 'n.nid = m.etid');
    $query->condition('n.type', $group_bundle);

    $etids = array_keys($query->execute()
      ->fetchAllAssoc('etid'));

    if (count($etids)) {
      $children = array();
      foreach ($etids as $etid) {
        $children = array_merge($children, $this->getGroupChildren($etid));
      }

      return array_merge($etids, $children);
    }
    else {
      return $etids;
    }
  }

}
