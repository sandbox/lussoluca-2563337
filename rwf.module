<?php

/**
 * @file
 * A configurable workflow managed by a hierarchy of editorial roles.
 * Allows to have multiple versions of the same node, each of which can be
 * revised independently.
 */

use Drupal\rwf\RwfServiceProvider;
use Drupal\rwf\StateMachine\VersionsManager;
use Drupal\rwf\StateMachine\WorkflowManager;
use Wellnet\StateMachine\StateMachineEvents;
use Drupal\rwf\Workflow\ScheduledTransitionExecutor;
use Drupal\rwf\Workflow\WorkflowOperationDescription;

/**
 * Implements hook_init().
 */
function rwf_init() {
  if (class_exists('Drupal\rwf\RwfServiceProvider')) {
    $dispatcher = rwf_pimple_container_get_instance('rwf.event_dispatcher');
    $dispatcher->addListener(StateMachineEvents::TRANSITION_SUCCEEDED, '_rwf_update_alias');
    $dispatcher->addListener(VersionsManager::COLLATERAL_EXECUTED, '_rwf_update_index_solr');
  }
}

/**
 *  Implements hook_menu().
 */
function rwf_menu() {
  $items = array();

  // Node level routes
  $items['node/%node/versions'] = array(
    'title' => 'Elenco versioni',
    'page callback' => 'rwf_versions_overview',
    'page arguments' => array(1),
    'access callback' => 'rwf_is_user_allowed',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );
  $items['node/%node/versions/fork'] = array(
    'title' => 'Nuova versione',
    'page callback' => 'rwf_version_fork',
    'page arguments' => array(1),
    'access callback' => 'rwf_version_fork_access',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
  );

  // Version level routes
  $items['node/%node/versions/%rwf_version/operations'] = array(
    'title' => 'Storico',
    'page callback' => 'rwf_operations_overview',
    'page arguments' => array(1, 3),
    'access callback' => 'rwf_revision_history_access',
    'access arguments' => array(1, 3),
    'weight' => -30,
    'type' => MENU_LOCAL_TASK,
    'tab_parent' => 'node/%/versions',
  );
  $items['node/%node/versions/%rwf_version/edit'] = array(
    'title' => 'Modifica',
    'page callback' => 'rwf_revision_create',
    'page arguments' => array(1, 3),
    'access callback' => 'rwf_revision_create_access',
    'access arguments' => array(1, 3),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'tab_parent' => 'node/%/versions',
  );
  $items['node/%node/versions/%rwf_version/transition'] = array(
    'title' => 'Cambia stato',
    'page callback' => 'rwf_workflow_transition',
    'page arguments' => array(3),
    'access callback' => 'rwf_workflow_transition_access',
    'access arguments' => array(1, 3),
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
    'tab_parent' => 'node/%/versions',
  );
  $items['node/%node/versions/%rwf_version/delete-scheduling'] = array(
    'title' => 'Rimuovi dalla schedulazione',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rwf_scheduling_delete_confirm', 1, 3),
    'access callback' => 'rwf_scheduling_delete_access',
    'access arguments' => array(1, 3),
    'type' => MENU_LOCAL_TASK,
    'weight' => -40,
    'tab_parent' => 'node/%/versions',
  );

  // Operation level routes
  $items['node/%node/versions/%/operations/%'] = array(
    'page callback' => 'rwf_operation_admin_preview',
    'page arguments' => array(3, 5),
    'access callback' => 'rwf_is_user_allowed',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
  );
//  TODO: check this
//  $items['node/%node/versions/%/operations/%/admin_preview'] = array(
//    'type' => MENU_DEFAULT_LOCAL_TASK,
//  );
  $items['node/%node/versions/%/operations/%/view'] = array(
    'page callback' => 'rwf_operation_view',
    'page arguments' => array(3, 5),
    'access callback' => 'rwf_is_user_allowed',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
  );

  // Admin routes
  $items['admin/config/content/rwf'] = array(
    'title' => 'Revision Workflow',
    'description' => 'Choose the content types that need a workflow and set all workflow options',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rwf_content_types_page'),
    'access arguments' => array('administer site configuration'),
    'file' => 'rwf.admin.inc',
  );
  $items['admin/config/content/rwf/content-types'] = array(
    'title' => 'Content types',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/config/content/rwf/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rwf_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'rwf.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Load function for "version" route component. Return FALSE if version is
 * NULL or non numeric.
 *
 * @param null|int $value
 *
 * @return bool|int
 */
function rwf_version_load($value = NULL) {
  return ($value != NULL && is_numeric($value)) ? $value : FALSE;
}

/**
 *  Implements hook_menu_alter().
 *
 *  Change callbacks for existing node paths so that we properly control
 *  revision-related operations.
 */
function rwf_menu_alter(&$items) {
  $items['node/%node']['access callback'] = '_rwf_node_access';
  $items['node/%node']['page callback'] = '_rwf_node_view';

  // edit route can't be unset in order to allow the editing of nodes without a
  // workflow
  $items['node/%node/edit']['access callback'] = '_rwf_node_edit_access';

  // Remove core revisions page
  unset($items['node/%node/revisions']);
  unset($items['node/%node/revisions/list']);
  unset($items['node/%node/revisions/view']);
  unset($items['node/%node/revisions/view/latest']);
  unset($items['node/%node/revisions/%/view']);
  unset($items['node/%node/revisions/%/revert']);
  unset($items['node/%node/revisions/%/delete']);
}

/**
 * Implements hook_admin_paths().
 */
function rwf_admin_paths() {
  if (variable_get('node_admin_theme')) {
    $paths = array(
      'node/*/versions' => TRUE,
      'node/*/versions/*' => TRUE,
      'node/*/versions/*/operations/*/view' => FALSE,
      'admin/config/content/rwf' => TRUE,
      'admin/config/content/rwf/*' => TRUE,
    );
    return $paths;
  }
}

/**
 * Implements of hook_ctools_plugin_directory().
 */
function rwf_ctools_plugin_directory($module, $plugin) {
  if ($module == 'entityreference') {
    return "plugins/entityreference/$plugin";
  }
}

/**
 * Menu callback: Returns a table that contains all version of a given node
 *
 * @param $node
 *
 * @return mixed
 */
function rwf_versions_overview($node) {
  drupal_set_title($node->title);

  $header = array(
    t('WID'),
    t('STATO'),
    t('ULTIMA MODIFICA'),
    array('data' => t('AZIONI'), 'colspan' => 2)
  );
  $rows = array(); // TODO check group permissions

  /** @var VersionsManager $versionsManager */
  $versionsManager = rwf_pimple_container_get_instance('rwf.versions_manager');
  $versions = $versionsManager->getNodeVersions($node);
  foreach ($versions as $version) {
    $row = array();
    $row[] = $version->wid;
    $row[] = t($version->state, array(), array('context' => 'rwf'));
    $row[] = format_date($version->changed, 'short');
    $row[] = l(t('Dettaglio'), "node/$node->nid/versions/$version->wid/operations");
    $row[] = '';
    $rows[] = $row;
  }

  if ($versionsManager->isNodeForkable($node)) {
    $rows[0][4] = l(t('Nuova versione'), "node/$node->nid/versions/fork");
  }

  $build['caption'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Elenco delle versioni') . '</h2>',
  );

  $build['node_versions_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}

/**
 * Access callback: checks whether a new version of the node can be created and
 * whether the user is allowed to do it
 *
 * @param $node
 *   A node object.
 *
 * @return bool
 *   TRUE if the operation may be performed, FALSE otherwise.
 *
 * @see VersionsManager
 */
function rwf_version_fork_access($node) {
  if (!rwf_is_user_allowed($node)) {
    return FALSE;
  }

  /** @var VersionsManager $versionsManager */
  $versionsManager = rwf_pimple_container_get_instance('rwf.versions_manager');
  return $versionsManager->isNodeForkable($node);
}

/**
 * Menu callback: Returns the node edit form to create a new version by forking
 * a previous one.
 *
 * @param $node
 *   A node object.
 *
 * @return array|mixed
 *   The form array.
 */
function rwf_version_fork($node) {
  drupal_set_title(t('Crea una nuova versione di @title', array(
    '@title' => $node->title
  )));
  return rwf_node_edit_form($node, 'version_fork');
}

/**
 * Access callback: checks whether a new revision of the node can be created and
 * whether the user is allowed to do it
 *
 * @param $node
 *   A node object.
 *
 * @return bool
 *   TRUE if the operation may be performed, FALSE otherwise.
 *
 * @see VersionsManager
 */
function rwf_revision_create_access($node, $wid) {
  if (!$wid) {
    return FALSE;
  }

  if (!rwf_is_user_allowed($node)) {
    return FALSE;
  }

  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);
  return $workflowManager->isVersionEditable();
}

/**
 * Menu callback: Returns a node edit form to create a new revision of the
 * given version.
 *
 * @param $node
 *   A node object.
 * @param $wid
 *   ID of the workflow related to a node version.
 *
 * @return array|mixed
 */
function rwf_revision_create($node, $wid) {
  drupal_set_title(t('Modifica revisione del @changed', array(
    '@changed' => $node->title
  )));
  return rwf_node_edit_form($node, 'revision_create');
}


/**
 * Access callback: checks whether there's a scheduling to delete
 *
 * @param $node
 *   A node object.
 * @param $wid
 *   ID of the current workflow
 *
 * @return bool
 *   TRUE if the operation may be performed, FALSE otherwise.
 */
function rwf_scheduling_delete_access($node, $wid) {
  if (!$wid) {
    return FALSE;
  }

  if (!rwf_is_user_allowed($node)) {
    return FALSE;
  }

  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);
  $schedules = $workflowManager->getSchedules();
  return !empty($schedules);
}


/**
 * Wrapper of the core form constructor.
 *
 * @param $node
 * @param null $action
 *
 * @return array|mixed
 */
function rwf_node_edit_form($node, $action = NULL) {
  // Embedding a node creation form has gotten more complex as
  // you need to use form_load_include. If you don't do this
  // (and only include node.pages.inc via module_load_include)
  // ajax on 'add another' fields will break with 500 errors.
  $form_state['build_info']['args'] = array($node, $action);
  form_load_include($form_state, 'inc', 'node', 'node.pages');
  return drupal_build_form($node->type . '_node_form', $form_state);
}

/**
 * Implements hook_form_alter.
 *
 * Alter only forms for nodes managed by rwf.
 */
function rwf_form_alter(&$form, &$form_state, $form_id) {

  // check if this is a form for a node managed by rwf
  if (isset($form['#node']) && variable_get('rwf_enable_' . $form['#node']->type, 0)) {

    // Remove core revisions widget
    unset($form['revision_information']);

    // the action determines some changes on node forms.
    // possible values are: create, admin_preview, revision_create, version_fork
    $action = '';
    $args = $form_state['build_info']['args'];
    if (isset($args[1])) {
      $action = $args[1];
    }
    // we infer that it's a node creation due to the lack of a nid
    elseif (!isset($form['nid']['#value'])) {
      $action = 'create';
    }

    if ($action != "") {
      // disable all fields in case of admin preview of the node
      if ($action === 'admin_preview') {
        foreach (element_children($form) as $field) {
          $form[$field]['#disabled'] = TRUE;
        }
      }
      else {
        $form['revision_comment'] = array(
          '#type' => 'textarea',
          '#title' => t('Inserisci un commento'),
          '#weight' => 15,
        );
      }

      $form['actions']['submit']['#submit'][] = 'rwf_node_edit_form_submit';
    }
  }
}

/**
 * Redirect the user to the versions overview.
 *
 * @param $form
 * @param $form_state
 */
function rwf_node_edit_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'node/' . $form_state['nid'] . '/versions';
}

/**
 * Menu callback: Returns a table that contains all operations executed on a
 * given version.
 *
 * @param $node
 * @param $wid
 *
 * @return mixed
 */
function rwf_operations_overview($node, $wid) {
  drupal_set_title($node->title);

  $header = array(
    t('Data'),
    t('Stato'),
    t('Attore del cambio di stato'),
    t('Commenti'),
    array('data' => t('Azioni'), 'colspan' => 2),
  );
  $rows = array(); // TODO check group permissions

  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);
  $operations = $workflowManager->getAllOperations();
  foreach ($operations as $operation) {
    $row = array();
    $row[] = format_date($operation->changed, 'short');
    $row[] = new WorkflowOperationDescription($operation);
    $row[] = user_load($operation->uid)->name;
    $row[] = $operation->comment;
    $row[] = l(t('Visualizza'), "node/$node->nid/versions/$operation->wid/operations/$operation->lid/view");
    $row[] = l(t('Preview admin'), "node/$node->nid/versions/$operation->wid/operations/$operation->lid/admin_preview");
    $rows[] = $row;
  }

  $build['version'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Versione ') . $wid . '</h2>',
  );

  $build['state'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Stato attuale: ') . t($workflowManager->getWorkflow()->state, array(), array('context' => 'rwf')) . '</h2>',
  );

  $build['caption'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Elenco delle revisioni') . '</h2>',
  );

  $build['node_operations_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}

/**
 * Access callback
 *
 * @param $node
 *   A node object.
 * @param $wid
 *
 * @return bool
 *
 * @see VersionsManager
 */
function rwf_revision_history_access($node, $wid) {
  if (!$wid) {
    return FALSE;
  }

  return rwf_is_user_allowed($node);
}

/**
 * Menu callback: displays the node as it was after the execution of an
 * operation.
 *
 * @param $wid
 * @param $lid
 *   id of the operation
 *
 * @return array
 *
 * @see node_view_multiple
 */
function rwf_operation_view($wid, $lid) {
  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);
  $operationInfo = $workflowManager->getOperationInfo($lid);

  drupal_set_title($workflowManager->getNode()->title);

  $build['version'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Versione ') . $wid . '</h2>',
  );

  $build['caption'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Visualizza revisione del %date', array('%date' => format_date($operationInfo['timestamp'], 'short'))) . '</h2>',
  );

  $build['node'] = node_view_multiple(array($operationInfo['node']->nid => $operationInfo['node']), 'full');

  return $build;
}

/**
 * Menu callback: returns a read-only (i.e. disabled) form that displays the
 * properties of the node at the time of a given operation (revision, transition
 * , ...)
 *
 * @param $wid
 * @param $lid
 *   ID of the operation
 * @return array|mixed
 */
function rwf_operation_admin_preview($wid, $lid) {
  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);
  $operationInfo = $workflowManager->getOperationInfo($lid);

  drupal_set_title($workflowManager->getNode()->title);

  $build['version'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Versione ') . $wid . '</h2>',
  );

  $build['caption'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Preview della revisione del %date', array('%date' => format_date($operationInfo['timestamp'], 'short'))) . '</h2>',
  );

  $build['form'] = rwf_node_edit_form($operationInfo['node'], 'admin_preview');

  return $build;
}

/**
 * Access callback: checks whether a transition can be performed
 *
 * @param $node
 *   A node object.
 * @param $wid
 * @return bool
 *   TRUE if the transition may be performed, FALSE otherwise.
 *
 * @see VersionsManager
 */
function rwf_workflow_transition_access($node, $wid) {
  if (!$wid) {
    return FALSE;
  }

  if (!rwf_is_user_allowed($node)) {
    return FALSE;
  }

  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);
  $schedules = $workflowManager->getSchedules();
  return empty($schedules);
}


/**
 * Menu callback: Displays a form to execute or schedule state changes.
 *
 * @param $wid
 * @return array|mixed
 */
function rwf_workflow_transition($wid) {

  $extra['wid'] = $wid;
  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);
  $extra['workflowManager'] = $workflowManager;

  drupal_set_title($workflowManager->getNode()->title);

  $build['version'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Versione ') . $wid . '</h2>',
  );

  $build['caption'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Cambia stato di una revisione') . '</h2>',
  );

  $build['state'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Stato attuale: ') . t($workflowManager->getWorkflow()->state, array(), array('context' => 'rwf')) . '</h2>',
  );

  $build['form'] = drupal_get_form('rwf_workflow_transition_form', $extra);

  return $build;
}

/**
 * Form constructor for the transition form.
 *
 * @param $form
 * @param $form_state
 * @param $extra
 * @return mixed
 */
function rwf_workflow_transition_form($form, &$form_state, $extra) {

  /** @var WorkflowManager $workflowManager */
  $workflowManager = $extra['workflowManager'];
  $allowedInputs = drupal_map_assoc(array_keys($workflowManager->getAllowedInputs()), function ($i) {
    return t($i, array(), array('context' => 'rwf'));
  });
  $form_state['storage']['rwf']['workflowManager'] = $workflowManager;

  $form['wid'] = array(
    '#type' => 'value',
    '#value' => $extra['wid'],
  );
  $form['state'] = array(
    '#type' => 'value',
    '#value' => $workflowManager->getWorkflow()->state,
  );
  $form['input'] = array(
    '#title' => t('Cambia stato in:'),
    '#type' => 'select',
    '#options' => $allowedInputs,
  );
  $form['scheduler'] = array(
    '#type' => 'fieldset',
  );
  $form['scheduler']['scheduling'] = array(
    '#type' => 'radios',
    '#title' => t('Quando?'),
    '#options' => array(
      t('Subito'),
      t('Il giorno')
    ),
    '#default_value' => 0,
  );
  $tomorrow = time() + 86400;
  $form['scheduler']['scheduling_time'] = array(
    '#type' => 'date',
    '#default_value' => array(
      'month' => format_date($tomorrow, 'custom', 'n'),
      'day' => format_date($tomorrow, 'custom', 'j'),
      'year' => format_date($tomorrow, 'custom', 'Y'),
    ),
  );
  $form['revision_comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Inserisci un commento'),
  );

  // the last lid is necessary to prevent concurrent changes on this workflow.
  // see rwf_workflow_transition_form_validate()
  $form['lastOperationId'] = array(
    '#type' => 'hidden',
    '#value' => current($workflowManager->getAllOperations())->lid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Cambia stato')
  );

  // disable submission if the current user is not allowed to perform transitions
  if (count($allowedInputs) === 0) {
    $form['submit']['#disabled'] = TRUE;
    form_set_error('input', t('Non sei autorizzato a cambiare lo stato attuale'));
  }

  return $form;
}

/**
 * Checks that the scheduling time, if sent by
 * rwf_workflow_transition_form(), is not before tomorrow.
 *
 * Prevents concurrent changes on this workflow.
 *
 * @param $form
 * @param $form_state
 *
 * @see rwf_workflow_transition_form()
 */
function rwf_workflow_transition_form_validate($form, &$form_state) {

  // check if a concurrent operation has been executed on this workflow (i.e.
  // something happened after the form was rendered but before it was submitted)
  if ($form_state['values']['lastOperationId'] > $_POST['lastOperationId']) {
    form_set_error('changed', t('The content on this page has either been modified by another user, or you have already submitted modifications using this form. As a result, your transition cannot be started.'));
  }

  // validate scheduling time
  if (isset($form_state['values']['scheduling']) && $form_state['values']['scheduling']) {
    $schedulingTime = $form_state['values']['scheduling_time'];
    // cache the scheduling time for the submit handler
    $parsedSchedulingTime = &drupal_static('rwf.scheduling-time');
    $parsedSchedulingTime = empty($schedulingTime) ?
      NULL : mktime(6, 0, 0, $schedulingTime['month'], $schedulingTime['day'], $schedulingTime['year']);
    if ($parsedSchedulingTime <= time()) {
      form_set_error('scheduling_time', t('Non è possibile schedulare transizioni prima di domani'));
    }
  }
}

/**
 * Executes a transition.
 *
 * A successful transition prevents concurrent changes to the node.
 *
 * @param $form
 * @param $form_state
 */
function rwf_workflow_transition_form_submit($form, &$form_state) {

  $wid = $form_state['values']['wid'];
  $input = $form_state['values']['input'];
  $comment = $form_state['values']['revision_comment'];
  $parsedSchedulingTime = NULL;
  if (isset($form_state['values']['scheduling']) && $form_state['values']['scheduling']) {
    $parsedSchedulingTime = &drupal_static('rwf.scheduling-time');
  }

  /** @var WorkflowManager $workflowManager */
  $workflowManager = $form_state['storage']['rwf']['workflowManager'];
  $workflowManager->executeTransition($input, $comment, $parsedSchedulingTime);

  // update node.changed to leverage the core optimistic lock and prevent
  // concurrent changes
  db_update('node')
    ->fields(array('changed' => REQUEST_TIME))
    ->condition('nid', $workflowManager->getNode()->nid)
    ->execute();

  $form_state['redirect'] = 'node/' . $workflowManager->getWorkflow()->nid . '/versions/' . $wid . '/operations';
}

/**
 * We replace node access callback with our _rwf_node_access() function, but a
 * lot of code directly calls the node_access() function to determine node
 * access, so we have to also implement this hook with similar logic.
 *
 * Implements hook_node_access().
 */
function rwf_node_access($node, $op, $account) {
  $access = NODE_ACCESS_IGNORE;

  if (rwf_is_managed_type($node)) {
    // access callbacks are executed before hook_init is triggered: we have to
    // manually register the autoloader
    composer_manager_register_autoloader();

    // Load public version
    $versionsManager = rwf_pimple_container_get_instance('rwf.versions_manager');
    $publicVersion = &drupal_static('rwf.public-version');
    // store the public version into the cache
    $publicVersion = $versionsManager->getPublicVersion($node);

    if (rwf_is_user_allowed($node)) {
      return NODE_ACCESS_ALLOW;
    }
    // if the user is not allowed to see the node, check if there is a
    // previously published version that he can see
    else {
      return isset($publicVersion) ? NODE_ACCESS_ALLOW : NODE_ACCESS_DENY;
    }
  }

  return $access;
}

/**
 *  Implements hook_node_insert().
 */
function rwf_node_insert($node) {
  if (rwf_is_managed_type($node)) {
    /** @var WorkflowManager $workflowManager */
    $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
    $workflowManager->initFromNode($node);
    $workflowManager->createVersion();
  }
}

/**
 *  Implements hook_node_update().
 */
function rwf_node_update($node) {
  if (rwf_is_managed_type($node)) {

    // don't handle node updates triggered by the WorkflowManager
    // TODO replace path matching with a better solution
    $currentPath = current_path();
    if (drupal_match_path($currentPath, 'node/*/versions/*/edit') ||
      drupal_match_path($currentPath, 'node/*/versions/fork')
    ) {
      /** @var WorkflowManager $workflowManager */
      $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
      if (drupal_match_path($currentPath, 'node/*/versions/fork')) {
        $workflowManager->initFromNode($node);
        $workflowManager->createVersion($node->old_vid);
      }
      else {
        $workflowManager->updateNode($node);
        $workflowManager->createRevision();
      }
    }
  }
}

/**
 * Implements hook_node_delete().
 */
function rwf_node_delete($node) {
  if (rwf_is_managed_type($node)) {
    /** @var VersionsManager $versionsManager */
    $versionsManager = rwf_pimple_container_get_instance('rwf.versions_manager');
    $versionsManager->deleteNodeVersions($node);
  }
}

/**
 * Menu callback: returns the confirm form to delete a scheduling.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function rwf_scheduling_delete_confirm($form, &$form_state) {
  $node = $form_state['build_info']['args'][0];
  $wid = $form_state['build_info']['args'][1];

  // pass values to the submit handler
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['wid'] = array(
    '#type' => 'value',
    '#value' => $wid,
  );

  $form['revision_comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Inserisci un commento'),
  );

  /** @var WorkflowManager $workflowManager */
  $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
  $workflowManager->initFromWid($wid);

  return confirm_form(
    $form,
    t('Vuoi eliminare la schedulazione per questo stato e riportare a @state?', array(
      '@state' => $workflowManager->getWorkflow()->state
    )),
    'node/' . $node->nid,
    '',
    t('Annulla la schedulazione')
  );
}

/**
 * Submit handler for rwf_scheduling_delete_confirm.
 * Deletes
 *
 * @param $form
 * @param $form_state
 *
 * @see rwf_scheduling_delete_confirm
 */
function rwf_scheduling_delete_confirm_submit($form, &$form_state) {
  $nid = $form_state['values']['nid'];
  $wid = $form_state['values']['wid'];
  $comment = $form_state['values']['revision_comment'];

  if ($form_state['values']['confirm']) {
    /** @var WorkflowManager $workflowManager */
    $workflowManager = rwf_pimple_container_get_instance('rwf.workflow_manager');
    $workflowManager->initFromWid($wid);
    $workflowManager->deleteScheduling($comment);
    drupal_set_message(t('Schedulazione annullata'));
  }

  $form_state['redirect'] = 'node/' . $nid . '/versions/' . $wid . '/operations';
}

/**
 * Implements hook_cron().
 *
 * Executes all scheduled transitions
 */
function rwf_cron() {
  // cron is executed during site install: we have to
  // manually register the autoloader
  composer_manager_register_autoloader();

  // The WorkflowManager is not invoked to execute scheduled transitions because
  // it has already processed all inputs in order to prevent the invalid ones
  /** @var ScheduledTransitionExecutor $executor */
  $executor = rwf_pimple_container_get_instance('rwf.scheduled_transitions_executor');

  // check if the scheduling hour has been forced and invoke the executor
  $schedulingHour = variable_get('rwf_scheduling_hour', NULL);
  $executor->execute($schedulingHour);
}

/**
 * A simple wrapper of pimple_container_get_instance() to ensure that
 * RwfServiceProvider is registered.
 *
 * @param $name
 *   name of the service
 * @return mixed|null
 *   Get a service from the Pimple container.
 *
 * @see pimple_container_register_provider
 * @see pimple_container_get_instance
 * @see RwfServiceProvider
 */
function rwf_pimple_container_get_instance($name) {
  $isRegistered = &drupal_static(__FUNCTION__, FALSE);

  if (!$isRegistered) {
    pimple_container_register_provider(new RwfServiceProvider());
    $isRegistered = TRUE;
  }

  return pimple_container_get_instance($name);
}

/**
 * Implements hook_views_api().
 */
function rwf_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function rwf_flag_default_flags() {
  $flags = array();
  $flags['archived'] = array(
    'entity_type' => 'node',
    'title' => 'archived',
    'global' => 1,
    'types' => array(),
    'flag_short' => 'Move this content to the archive',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove this content from the archive',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'api_version' => 3,
  );

  return $flags;
}

/**
 * Implements hook_node_presave().
 */
function rwf_node_presave($node) {

  // Check module exists 'pathauto'
  if (module_exists('pathauto')) {

    // Checks if the node is of a content type managed by rwf
    if (rwf_is_managed_type($node)) {
      $node->path['pathauto'] = FALSE; // Disable auto pathauto
    }
  }
}

/**
 * Implements hook_tokens_alter().
 */
function rwf_tokens_alter(array &$replacements, array $context) {
  $options = $context['options'];
  $sanitize = !empty($options['sanitize']);

  // Node token fixes.
  if ($context['type'] == 'node' && !empty($context['data']['node'])) {
    $node = $context['data']['node'];

    // Check if ct is in workflow
    if (rwf_is_managed_type($node)) {

      foreach ($context['tokens'] as $name => $original) {
        switch ($name) {
          case 'title':

            // Get public version of node
            $versionsManager = rwf_pimple_container_get_instance('rwf.versions_manager');
            $publicVersion = &drupal_static('rwf.public-version');
            $publicVersion = $versionsManager->getPublicVersion($node);

            // If found public version, change title
            if (isset($publicVersion)) {
              $node_public = node_load($publicVersion->nid, $publicVersion->vid);
              $replacements[$original] = $sanitize ? check_plain($node_public->title) : $node_public->title;
            }
            break;
        }
      }// end/foreach
    }
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function rwf_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'entity_info_alter') {
    // Move our hook implementation to the bottom.
    $group = $implementations['rwf'];
    unset($implementations['rwf']);
    $implementations['rwf'] = $group;
  }
}

/**
 * Implements hook_entity_info_alter().
 */
function rwf_entity_info_alter(&$entity_info) {
  if(module_exists('entity')) {
    $entity_info['node']['access callback'] = 'rwf_entity_access';
  }
}

/* API */

/**
 * Returns the public version of a node.
 *
 * @param $node
 *
 * @return object
 */
function rwf_get_public_version($node) {
  if (rwf_is_managed_type($node)) {
    $versionsManager = rwf_pimple_container_get_instance('rwf.versions_manager');
    $publicVersion = $versionsManager->getPublicVersion($node);

    if (isset($publicVersion)) {
      $node = node_load($publicVersion->nid, $publicVersion->vid);
    }
  }

  return $node;
}

/**
 * Returns true if the user is allowed to manage the given $node
 *
 * @param $node
 *
 * @return bool
 */
function rwf_is_user_allowed($node) {
  $rights = &drupal_static(__FUNCTION__, array());

  if (isset($rights[$node->nid])) {
    return $rights[$node->nid];
  }

  if (!user_is_logged_in()) {
    $rights[$node->nid] = FALSE;
    return FALSE;
  }

  if (!rwf_is_managed_type($node)) {
    $rights[$node->nid] = FALSE;
    return FALSE;
  }

  $context = array(
    'user' => $GLOBALS['user'],
    'node' => $node,
  );

  // access callbacks are executed before hook_init is triggered: we have to
  // manually register the autoloader
  composer_manager_register_autoloader();

  /** @var \Wellnet\StateMachine\ConfiguratorInterface $configurator */
  $configurator = rwf_pimple_container_get_instance('rwf.configurator');
  $config = $configurator->getConfig();
  $node_access = $config['node_access'];

  /** @var \Wellnet\StateMachine\GuardInterface $node_access_guard */
  $node_access_guard = (new \ReflectionClass($node_access['class']))->newInstance($node_access['args']);
  $rights[$node->nid] = $node_access_guard->allow($context);

  drupal_alter('rwf_is_user_allowed', $context, $rights[$node->nid]);

  return $rights[$node->nid];
}

/**
 * Checks if the node is of a content type managed by rwf.
 *
 * @param $node
 *   A node object.
 * @return bool
 *
 */
function rwf_is_managed_type($node) {
  $type = $node;
  if(is_object($node)) {
    $type = $node->type;
  }

  return variable_get('rwf_enable_' . $type, 0);
}

/* Private functions */

/**
 * Access callback: checks whether the current user may see the given node.
 *
 * If the node is managed by a workflow, it means that:
 * - the user belongs to the same group of the node (or to one of its ancestors)
 * - there is a public version of the node
 *
 * @param $op
 * @param $node
 *
 * @return bool
 *
 * @see rwf_is_user_allowed
 * @see _rwf_node_view()
 */
function _rwf_node_access($op, $node) {
  $rights = &drupal_static(__FUNCTION__, array());

  // If we've already checked access for this node and op, return from cache.
  if (isset($rights[$node->nid])) {
    return $rights[$node->nid];
  }

  if (rwf_is_managed_type($node)) {
    // access callbacks are executed before hook_init is triggered: we have to
    // manually register the autoloader
    composer_manager_register_autoloader();

    // Load public version
    $versionsManager = rwf_pimple_container_get_instance('rwf.versions_manager');
    $publicVersion = &drupal_static('rwf.public-version');
    // store the public version into the cache
    $publicVersion = $versionsManager->getPublicVersion($node);

    if (rwf_is_user_allowed($node)) {
      $rights[$node->nid] = TRUE;
      return TRUE;
    }
    // if the user is not allowed to see the node, check if there is a
    // previously published version that he can see
    else {
      $rights[$node->nid] = isset($publicVersion);
      return $rights[$node->nid];
    }
  }
  else {
    $rights[$node->nid] = node_access($op, $node);
    return $rights[$node->nid];
  }
}

/**
 * Menu callback: Displays a single node.
 * If the node has a workflow and the user is not allowed to see it (eg the node
 * it's not yet published), the function will display the latest published
 * version of the same node.
 *
 * @param $node
 *
 * @return array
 *   A page array suitable for use by drupal_render().
 *
 * @see _rwf_node_access()
 */
function _rwf_node_view($node) {
  if (rwf_is_managed_type($node)) {
    // retrieve the public version from the cache if public version exist (load in _rwf_node_access())
    $publicVersion = &drupal_static('rwf.public-version');
    if (isset($publicVersion)) {
      $node = node_load($publicVersion->nid, $publicVersion->vid);
    }
  }

  return node_page_view($node);
}


/**
 * Disables operations on nodes with a workflow, otherwise leverages node_access
 *
 * @param $node
 *
 * @return bool
 */
function _rwf_node_edit_access($op, $node) {
  return rwf_is_managed_type($node) ? FALSE : node_access($op, $node);
}

/**
 * Function execute after trigger event "TRANSITION_SUCCEEDED".
 *
 * @param $transition_event
 */
function _rwf_update_alias($transition_event) {

  // Update alias with pathauto
  if (module_exists('pathauto')) {

    // Get destination state of event
    $state_destination = $transition_event->getTransition()
      ->getDestination()
      ->getName();

    // If state is Publish, update alias
    if ($state_destination == 'published') {

      // Recovery node from context
      $context = $transition_event->getContext();
      $node = $context['node'];

      // Update alias
      pathauto_node_update_alias($node, 'update');
    }
  }
}

/**
 * Function execute after event "VersionsManager::COLLATERAL_EXECUTED".
 *
 * @param \Drupal\rwf\StateMachine\CollateralExecutedEvent $collateral_event
 */
function _rwf_update_index_solr($collateral_event) {

  // Update index
  if (module_exists('search_api')) {

    // Get destination state of event
    $state_destination = $collateral_event->getState();

    // If state is published or archived or unpublished
    if ($state_destination == 'published' ||
      $state_destination == 'archived' ||
      $state_destination == 'unpublished'
    ) {

      $nid = $collateral_event->getNid();

      // Clear field cache for the node.
      cache_clear_all('field:node:' . $nid, 'cache_field');

      // Reindex the node.
      search_api_track_item_change('node', array($nid));
    }
  }
}

/**
 * Access callback for the node entity.
 *
 * Copied from entity_metadata_no_hook_node_access() because it uses directly
 * the private _node_revision_access() function that broke our version/revision
 * system.
 *
 * @see entity_metadata_no_hook_node_access()
 */
function rwf_entity_access($op, $node = NULL, $account = NULL) {
  // First deal with the case where a $node is provided.
  if (isset($node)) {
    if ($op == 'create') {
      if (isset($node->type)) {
        return _rwf_node_access($op, $node->type, $account);
      }
      else {
        throw new EntityMalformedException('Permission to create a node was requested but no node type was given.');
      }
    }
    // If a non-default revision is given, incorporate revision access.
    $default_revision = node_load($node->nid);
    if ($node->vid !== $default_revision->vid) {
      return _rwf_node_access($op, $node, $account);
    }
    else {
      return _rwf_node_access($op, $node, $account);
    }
  }
  // No node is provided. Check for access to all nodes.
  if (user_access('bypass node access', $account)) {
    return TRUE;
  }
  if (!user_access('access content', $account)) {
    return FALSE;
  }
  if ($op == 'view' && node_access_view_all_nodes($account)) {
    return TRUE;
  }
  return FALSE;
}
